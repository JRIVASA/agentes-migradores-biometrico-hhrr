Attribute VB_Name = "Modulo_Inicio"
Global Conexion As New ADODB.Connection

Global G_Server_Local
Global G_BDServer_Local
Global G_ServerLogin        As String
Global G_ServerPassword     As String

Global G_MostrarProgreso

Global G_Excedente
Global G_CargaDiaria
Global G_Desde
Global G_Tope
Global G_Gracia
Global G_TiempoConsideraHoraExtra As Single

Global G_HorasDiaDomingoT As Integer
Global G_HorasDiaFeriadoT As Integer
Global G_HorasDiaLibreT As Integer

Global G_FinJornadaDiurna As Date
Global G_FinJornadaNocturna As Date

Global G_MinutosSignificativos_HorasFaltantes As Integer
Global G_MinutosSignificativos_Medio_Dia As Integer
Global G_MinutosSignificativos_Dia As Integer

Global G_Accion_LlegadaTardia As Integer

Global G_FormatoHorasconMinutos As Boolean

Global MultiplesDiasLibres  As Boolean

Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Public Sub Crear_Conexiones(BaseDatos1 As String, SrvLogin As String, srvPassword As String, Optional Opcion As Boolean = 0)

    On Error GoTo ErrorAronio

    Dim ConexString1

    If Opcion = True Then
        ConexString1 = "Provider =MSDataShape.1;Persist Security Info=False;Data Source=" & G_Server_Local & ";User ID=" & SrvLogin & ";Password=" & srvPassword & ";Initial Catalog=" & G_BDServer_Local & ";Data Provider=SQLOLEDB.1"
    Else
        ConexString1 = "Driver={SQL Server};Server=" & G_Server_Local & ";Database=" & G_BDServer_Local & ";Uid=" & SrvLogin & ";Pwd=" & srvPassword & ";"
    End If
    
    If Conexion.State = adStateOpen Then Conexion.Close
    Conexion.ConnectionString = ConexString1
    
    If SrvLogin <> "" And G_BDServer_Local <> "" And G_Server_Local <> "" Then
        Conexion.Open
    Else
        MsgBox "No se ha definido el Servidor o la Base de Datos.", vbExclamation, "Informaci�n Stellar"
        End
    End If
    
    Exit Sub

ErrorAronio:
    
   Debug.Print err.Description
    MsgBox "No se puede localizar el servidor o la base de datos.", vbExclamation, "Informaci�n Stellar"
    End
    
End Sub

Public Function FiltrarMarcajes(rs As ADODB.Recordset, Rango As Integer) As ADODB.Recordset
    
    Dim RsFiltrados As New ADODB.Recordset
    
    While Not rs.EOF
        primerahora = Format(rs!Hora, "HH:mm:ss")
        rs.MoveNext
        If Not rs.EOF Then
            'Con la funcion DateAdd se suman los segundos del rango pasandole como primer parametro: "s" para que sume segundos,
            'luego se formatea para que solo devuelva el resultado en formato Hora.
            primeraHoraMasRango = Format(DateAdd("s", Rango, primerahora), "HH:mm:ss")
            
            If primeraHoraMasRango > Format(rs!Hora, "HH:mm:ss") Then
                Debug.Print "Si es mayor"
            Else
                Debug.Print "No es mayor"
            End If
        End If
    Wend
    
End Function

Public Sub Importar_A_Baseproc()

    Dim DiaLibre                As Boolean
    Dim rs                           As New ADODB.Recordset
    Dim Rs2                         As New ADODB.Recordset
    Dim Rss                         As New ADODB.Recordset
    Dim mRs                       As New ADODB.Recordset
    Dim Impar                    As Boolean
    Dim sql                      As String
    Dim Eliminar               As Integer
    Dim mVar                    As Variant
    Dim PreReg                 As Date

    
    

    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, True)
    
    sql = "update MA_CorrelativosMarcajes set fHasta = Null where fHasta = ''"
    Debug.Print sql
    Conexion.Execute sql
    
    Rss.CursorLocation = adUseServer
    If Rss.State = 1 Then Rss.Close
    Rss.Open "select * from baseproc where 1 = 2", Conexion, adOpenStatic, adLockBatchOptimistic, adCmdText
    
    'Aqui se traen los empleados que tienen correlativos y las nominas a las que pertenecen, asi como tambien la fecha desde y la fecha hasta del
    'correlativo del empleado y el Biometrico(s) a cual esta asociado.

'    SQL = "SELECT CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta, Rango, PrimerMarcaje1, UltimoMarcaje1, PrimerMarcaje2, UltimoMarcaje2, MarcajeAObviar " _
'        & "FROM (SELECT CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
'        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
'        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta, Rango, PrimerMarcaje1, UltimoMarcaje1, PrimerMarcaje2, UltimoMarcaje2,  " _
'        & "MarcajeAObviar FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB1.Rango, TB1.PrimerMarcaje1,  " _
'        & "TB1.UltimoMarcaje1, TB1.PrimerMarcaje2, TB1.UltimoMarcaje2, TB1.MarcajeAObviar, TB2.Nomina2, TB2.UltimaCorrida " _
'        & "FROM (SELECT  TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1, MIN(MA_CorrelativosMarcajes.fDesde) AS  " _
'        & "FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta, MA_ConfiguradorMarcajes.Rango, MA_ConfiguradorMarcajes.PrimerMarcaje1,  " _
'        & "MA_ConfiguradorMarcajes.UltimoMarcaje1, MA_ConfiguradorMarcajes.PrimerMarcaje2, MA_ConfiguradorMarcajes.UltimoMarcaje2, MA_ConfiguradorMarcajes.MarcajeAObviar " _
'        & "FROM TR_IMPORTAMARCAJE INNER JOIN MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN MA_CorrelativosMarcajes ON  " _
'        & "MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar INNER JOIN MA_ConfiguradorMarcajes ON MA_CorrelativosMarcajes.Biometrico = MA_ConfiguradorMarcajes.Identificador " _
'        & "WHERE (TR_IMPORTAMARCAJE.migrado = 0) GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta, MA_ConfiguradorMarcajes.Rango,  " _
'        & "MA_ConfiguradorMarcajes.PrimerMarcaje1, MA_ConfiguradorMarcajes.UltimoMarcaje1, MA_ConfiguradorMarcajes.PrimerMarcaje2, MA_ConfiguradorMarcajes.UltimoMarcaje2,  " _
'        & "MA_ConfiguradorMarcajes.MarcajeAObviar) AS TB1 LEFT OUTER JOIN (SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaFinPeriodo) AS UltimaCorrida FROM  MA_NOMINA_PROCESADAS " _
'        & "WHERE (cu_estatus = 'NOMINA') GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T "
       
      sql = "SELECT CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta, Rango, PrimerMarcaje1, UltimoMarcaje1, PrimerMarcaje2, UltimoMarcaje2, MarcajeAObviar " _
        & "FROM (SELECT CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta, Rango, PrimerMarcaje1, UltimoMarcaje1, PrimerMarcaje2, UltimoMarcaje2,  " _
        & "MarcajeAObviar FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB1.Rango, TB1.PrimerMarcaje1,  " _
        & "TB1.UltimoMarcaje1, TB1.PrimerMarcaje2, TB1.UltimoMarcaje2, TB1.MarcajeAObviar, TB2.Nomina2, TB2.UltimaCorrida " _
        & "FROM (SELECT  TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1, MIN(MA_CorrelativosMarcajes.fDesde) AS  " _
        & "FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta, MA_ConfiguradorMarcajes.Rango, MA_ConfiguradorMarcajes.PrimerMarcaje1,  " _
        & "MA_ConfiguradorMarcajes.UltimoMarcaje1, MA_ConfiguradorMarcajes.PrimerMarcaje2, MA_ConfiguradorMarcajes.UltimoMarcaje2, MA_ConfiguradorMarcajes.MarcajeAObviar " _
        & "FROM TR_IMPORTAMARCAJE INNER JOIN MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN MA_CorrelativosMarcajes ON  " _
        & "MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar INNER JOIN MA_ConfiguradorMarcajes ON MA_CorrelativosMarcajes.Biometrico = MA_ConfiguradorMarcajes.Identificador " _
        & "GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta, MA_ConfiguradorMarcajes.Rango,  " _
        & "MA_ConfiguradorMarcajes.PrimerMarcaje1, MA_ConfiguradorMarcajes.UltimoMarcaje1, MA_ConfiguradorMarcajes.PrimerMarcaje2, MA_ConfiguradorMarcajes.UltimoMarcaje2,  " _
        & "MA_ConfiguradorMarcajes.MarcajeAObviar) AS TB1 LEFT OUTER JOIN (SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaCorrida) AS UltimaCorrida FROM  MA_NOMINA_PROCESADAS " _
        & "WHERE (cu_estatus = 'NOMINA') GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T"
        
        '& IIf(G_Prueba, "", " WHERE (FechaDesde < FechaHasta)") '_
        '& " ORDER BY CodigoEmpleado, FechaDesde"

    Conexion.CommandTimeout = 0
    
    Debug.Print sql
    
    Set rs = Conexion.Execute(sql)
    Debug.Print rs!Rango
    
    numempleados = 0
    frm_AgenteMigradorMarcajes2.Show
    While Not rs.EOF
        numempleados = numempleados + 1
        frm_AgenteMigradorMarcajes2.Caption = "Clasificando marcajes a baseproc | Registro " & numempleados & "/" & rs.RecordCount
'        If Rs!codigoempleado = "11607640" Then
'            Debug.Print Rs!codigoempleado
'        End If
        
        mVar = DevolverCortes(CStr(rs!Nomina), CDate(rs!fechadesde), CDate(rs!FechaHasta))
        
        fdesde = mVar(0)
        'fHasta = mvar(1)

        fhasta = CDate(Format(rs!FechaHasta, "dd/mm/yyyy"))

        If CDate(fdesde) >= CDate(fhasta) Then fdesde = fhasta - 1

        'fDesde = CDate(Format(rs!FechaDesde, "dd/mm/yyyy"))
                
        'fHasta = CDate(Format(rs!FechaHasta, "dd/mm/yyyy"))
        
        'fHasta = CDate(Format(IIf(Rs!FechaHasta < Now, Rs!FechaHasta, DateAdd("d", -1, Now)), "dd/mm/yyyy"))
        
        If G_Tope <> "" Then fhasta = G_Tope
        If G_Desde <> "" Then fdesde = G_Desde
        If G_CargaDiaria = 1 Then fdesde = fhasta
        
        numerodias = DateDiff("d", fdesde, fhasta) + 1
        
        frm_AgenteMigradorMarcajes2.ProgressBar.Max = CInt(numerodias)
        frm_AgenteMigradorMarcajes2.ProgressBar.Value = 1
        
       ' If Rs!CodigoEmpleado = "20148534" Then
         '   Debug.Print "prueba"
     '   End If
        
        
        For i = CDate(fdesde) To CDate(fhasta)
                        
            If frm_AgenteMigradorMarcajes2.ProgressBar.Value = CInt(numerodias) Then
                frm_AgenteMigradorMarcajes2.ProgressBar.Value = 1
            Else
                frm_AgenteMigradorMarcajes2.ProgressBar.Value = frm_AgenteMigradorMarcajes2.ProgressBar.Value + 1
            End If
        
            msql = "SELECT TB1.*, TR_IMPORTAMARCAJE.biometrico AS Biometrico FROM (SELECT  cod, dia, hora, Tipo, Clasificacion " _
                & "FROM (SELECT tb.cod, tb.dia, tb.hora, CASE WHEN baseproc_1.bas_clasificacion IS NULL THEN '' ELSE baseproc_1.bas_clasificacion END AS Clasificacion,  " _
                & "CASE WHEN baseproc_1.bas_tipo_registro = 0 THEN 'MANUAL' ELSE 'AUTOMATICO' END AS Tipo FROM (SELECT codigoemple AS cod, diamarca AS dia,  " _
                & "horamarca AS hora FROM TR_IMPORTAMARCAJE WHERE (diamarca = '" & i & "') AND (codigoemple = '" & rs!CodigoEmpleado & "' ) and migrado = 0 and obviar = 0 UNION SELECT bas_claves AS cod, bas_fechas AS dia,  " _
                & "bas_horass AS hora FROM baseproc WHERE (bas_fechas = '" & i & "') AND (bas_claves = '" & rs!CodigoEmpleado & "' and bas_clasificacion <> 'INASISTENCIA')) AS tb LEFT OUTER JOIN baseproc AS baseproc_1 ON  " _
                & "tb.hora = baseproc_1.bas_horass and tb.dia = baseproc_1.bas_fechas and tb.cod = baseproc_1.bas_claves) AS tb2) AS TB1 LEFT OUTER JOIN TR_IMPORTAMARCAJE ON TB1.hora = TR_IMPORTAMARCAJE.horamarca " _
                & "AND TB1.COD = TR_IMPORTAMARCAJE.CODIGOEMPLE " _
                & "ORDER BY dia, hora "
           
            Debug.Print msql
            
            If mRs.State = 1 Then mRs.Close
            mRs.Open msql, Conexion, adOpenDynamic, adLockBatchOptimistic
            numregistros = 0
            
            If Not mRs.EOF Then
            Debug.Print mRs.RecordCount
                'If (rs!CodigoEmpleado = "14679885") Then 'Debugging...
                        'Debug.Print mSql
                'End If

                Debug.Print sql
                mRs.MoveFirst
                                
                While Not mRs.EOF
               
'                If mRs!cod = "11280403" Then
'                    Debug.Print "este"
'                End If
                
                 FechaActual = Format(mRs!Hora, "HH:mm:ss")

                    If numregistros = 0 Then
                        PreReg = Format(mRs!Hora, "HH:mm:ss")
'                        PreRegMasRango = Format(DateAdd("s", Rango, PreReg), "HH:mm:ss")
'                        PreRegMenosRango = Format(DateAdd("s", Rango * -1, PreReg), "HH:mm:ss")
                        numregistros = numregistros + 1
                        mRs.MoveNext
                    Else

                        'If Abs(DateDiff("s", PreRegMenosRango, FechaActual)) <= Rango Or Abs(DateDiff("s", PreRegMasRango, FechaActual)) <= Rango Then
                        If PreReg = mRs!Hora Then
                            mRs.Delete
                            mRs.MoveNext
                        Else
                            PreReg = mRs!Hora
'                            PreRegMasRango = Format(DateAdd("s", Rango, PreReg), "HH:mm:ss")
'                            PreRegMenosRango = Format(DateAdd("s", Rango * -1, PreReg), "HH:mm:ss")
                            numregistros = numregistros + 1
                            mRs.MoveNext
                        End If
                    End If
                Wend
                mRs.MoveFirst
'                Debug.Print mRs.RecordCount
            End If
'           Debug.Print SQL

            If Not mRs.EOF And numregistros > 1 Then
                Debug.Print "DELETE FROM baseproc WHERE bas_claves = '" & rs!CodigoEmpleado & "' and bas_fechas = '" & i & "' and bas_tipo_registro = 1"
                Conexion.Execute ("DELETE FROM baseproc WHERE bas_claves = '" & rs!CodigoEmpleado & "' and bas_fechas = '" & i & "' and bas_tipo_registro = 1")
                contador = 1
                Debug.Print msql
                If Not mRs.BOF Then mRs.MoveFirst

                While Not mRs.EOF
                    primerahora = Format(mRs!Hora, "HH:mm:ss")  'Se extrae el primer marcaje.
                    mRs.MoveNext                                'Se mueve el cursor del recordset al siguiente
                    If Not mRs.EOF Then                         'En caso de que hayan mas registros entonces se le suma el rango al primer marcaje y se compara con el siguiente
                        'Con la funcion DateAdd se suman los segundos del rango pasandole como primer parametro: "s" para que sume segundos,
                        'luego se formatea para que solo devuelva el resultado en formato Hora.
                        
                        If rs!Rango = 0 Or rs!Rango = "" Then
                            rs!Rango = 1200
                        End If

                        primeraHoraMasRango = Format(DateAdd("s", rs!Rango, primerahora), "HH:mm:ss")   'Aqui se le suma el rango al primer marcaje.

                        'Si la Hora mas el Rango > q la hora de marcaje quiere decir q el siguiente marcaje esta dentro del rango establecido
                        'en la configuraci�n, por lo tanto se obvia el marcaje que se haya configurado
                        'test = Format(mRs!hora, "HH:mm:ss")
                        'Debug.Print test
                        
                        If primeraHoraMasRango >= Format(mRs!Hora, "HH:mm:ss") Then
                            'Si contador es par quiere decir q es salida por lo tanto se verifica la configuracion para las salidas
                            If contador Mod 2 = 0 Then
                                If rs!PrimerMarcaje2 Then
                                    Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1, obviar = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                                    mRs.Delete          'Se borra el registro del recordset.
                                    mRs.MovePrevious    'Y se mueve el cursor al registro anterior para poder compararlo con el siguiente del que se borro.
                                Else
                                    mRs.MovePrevious    'Se mueve al registro anterior.
                                    Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1, obviar = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                                    mRs.Delete          'Se elimina el registro del recorset m�s no de la Base de Datos (esto es por la manera en que se abrio el recordset, que se puede editar sin afectar a la BD).
                                    mRs.MoveNext        'Y luego se vuelve a colocar en el registro siguiente que era en el que estaba parado el cursor.
                                End If
                            Else 'Si es entrada se repite el proceso pero considerando el marcaje a tomar segun la configuraci�n
                                If rs!PrimerMarcaje1 Then
                                    Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1, obviar = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                                    mRs.Delete          'Se borra el registro del recordset.
                                    mRs.MovePrevious    'Y se mueve el cursor al registro anterior para poder compararlo con el siguiente del que se borro.
                                Else
                                    mRs.MovePrevious    'Se mueve al registro anterior.
                                    Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1, obviar = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                                    mRs.Delete          'Se elimina el registro del recorset m�s no de la Base de Datos (esto es por la manera en que se abrio el recordset, que se puede editar sin afectar a la BD).
                                    mRs.MoveNext        'Y luego se vuelve a colocar en el registro siguiente que era en el que estaba parado el cursor.
                                End If
                            End If
                        Else
                            contador = contador + 1     'Aqui se incrementa el contador solo en caso de que no hayan marcajes juntos.
                        End If
                    End If
                Wend
    
                If Not mRs.BOF Then mRs.MoveFirst
                
                numregistros = mRs.RecordCount
                
                Eliminar = 0
                If numregistros Mod 2 <> 0 Then
                    Select Case rs!MarcajeAObviar
                        Case Is = "Primero"
                            Eliminar = 1
                        Case Is = "Segundo"
                            Eliminar = 2
                        Case Is = "Intermedio"
                            Eliminar = (numregistros / 2) + 0.5
                        Case Is = "Pen�ltimo"
                            Eliminar = numregistros - 1
                        Case Is = "�ltimo"
                            Eliminar = numregistros
                    End Select
    
                    For registro = 1 To numregistros
                        If registro = Eliminar Then
                            Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1, obviar = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                            mRs.Delete
                        End If
                        mRs.MoveNext
                    Next registro
                End If
                
                If Not mRs.BOF And mRs.RecordCount > 0 Then mRs.MoveFirst
            End If
            
            numregistros = 0
            If Not mRs.EOF Then
'                mRs.MoveFirst
'                While Not mRs.EOF
'                    numRegistros = numRegistros + 1
'                    mRs.MoveNext
'                Wend
'                mRs.MoveFirst
                numregistros = mRs.RecordCount
                Debug.Print mRs.RecordCount
            End If
                              
            Select Case numregistros
                Case Is <= 1
                Debug.Print "from baseproc where (bas_fechas = '" & i & "') AND (bas_claves = '" & rs!CodigoEmpleado & "')"
                    Debug.Print "delete from baseproc where (bas_fechas = '" & i & "') AND (bas_claves = '" & rs!CodigoEmpleado & "')"
                   Conexion.Execute ("delete from baseproc where (bas_fechas = '" & i & "') AND (bas_claves = '" & rs!CodigoEmpleado & "')")
                    
                    Dim Fields As ADODB.Fields
                    
'                    mRs.fields.Refresh
'                    For k = 0 To mRs.fields.Count - 1
'                       Debug.Print mRs.fields(k).Name
'
'                    Next k
                                       
                    Rss.AddNew
                    Rss("bas_fechas") = i
                    Rss("bas_claves") = rs!CodigoEmpleado
                    Rss("bas_horass") = i
                    Rss("bas_asistencia") = 0
                    Rss("bas_tipo_registro") = 1
                    Rss("bas_clasificacion") = "INASISTENCIA"
                    
                    If mRs.EOF Then
                        Rss("bas_biometrico") = ""
                    Else
                        Rss("bas_biometrico") = mRs!Biometrico
                    End If
                                                
                Case Is >= 2
                        For j = 1 To numregistros
                            If j = 1 And mRs!Tipo = "AUTOMATICO" Then 'Si es el primer registro quiere decir que es la hora de entrada ya que los registros estan ordenados por horas
                                Rss.AddNew
                                Rss("bas_fechas") = mRs!Dia
                                Rss("bas_claves") = mRs!cod
                                Rss("bas_horass") = mRs!Hora
                                Rss("bas_asistencia") = 0
                               Rss("bas_tipo_registro") = 1
                                Rss("bas_clasificacion") = "ENTRADA JORNADA"
                                Debug.Print mRs.Source
                                A = mRs!Clasificacion

                                Rss("bas_biometrico") = mRs!Biometrico
                                Debug.Print "UPDATE TR_IMPORTAMARCAJE SET migrado = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'"
                                Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")

                            ElseIf (j <> numregistros) And mRs!Tipo <> "MANUAL" Then
                                    If j Mod 2 = 0 Then
                                    
                                    Debug.Print j Mod 2
                                    Debug.Print j
                                        Rss.AddNew
                                        Rss("bas_fechas") = mRs!Dia
                                        Rss("bas_claves") = mRs!cod
                                        Rss("bas_horass") = mRs!Hora
                                        Rss("bas_asistencia") = 0
                                        Rss("bas_tipo_registro") = 1
                                        Rss("bas_clasificacion") = "SALIDA INTERMEDIA"
                                        Rss("bas_biometrico") = mRs!Biometrico
                                        Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                                    Else
                                    
                                    Debug.Print j
                                        Rss.AddNew
                                        Rss("bas_fechas") = mRs!Dia
                                        Rss("bas_claves") = mRs!cod
                                        Rss("bas_horass") = mRs!Hora
                                        Rss("bas_asistencia") = 0
                                        Rss("bas_tipo_registro") = 1
                                        Rss("bas_clasificacion") = "ENTRADA INTERMEDIA"
                                        Rss("bas_biometrico") = mRs!Biometrico
                                        Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                                    End If
                                    
                            ElseIf j = numregistros Then
                                Rss.AddNew
                                Rss("bas_fechas") = mRs!Dia
                                Rss("bas_claves") = mRs!cod
                                Rss("bas_horass") = mRs!Hora
                                Rss("bas_asistencia") = 0
                                Rss("bas_tipo_registro") = 1
                                Rss("bas_clasificacion") = "SALIDA JORNADA"
                                Rss("bas_biometrico") = mRs!Biometrico
                                Conexion.Execute ("UPDATE TR_IMPORTAMARCAJE SET migrado = 1 WHERE horamarca = '" & Format(mRs!Hora, "dd/mm/yyyy HH:MM:SS") & "' AND codigoemple = '" & mRs!cod & "' AND biometrico = '" & mRs!Biometrico & "'")
                            End If
                        mRs.MoveNext
                        Next j
            End Select
        Next i
        Rss.UpdateBatch
        mRs.Close
        rs.MoveNext
    Wend
    
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, False)
        
End Sub

Public Sub CargarInasistencias()

    Dim Dia                         As String
    Dim DiaLibre                    As Boolean
    Dim rs                          As New ADODB.Recordset
    Dim Rs2                         As New ADODB.Recordset
    Dim Rss                         As New ADODB.Recordset
    Dim mRs                         As New ADODB.Recordset
    Dim RsFeriados                  As New ADODB.Recordset
    Dim RsAsistencia                As New ADODB.Recordset
    Dim RsVacaciones                As New ADODB.Recordset
    Dim RsDiasLibresRotativo        As New ADODB.Recordset
    Dim CantidadRegistros   As Integer
    Dim mVar As Variant
        
    'Aqui se traen los empleados que tienen correlativos y las nominas a las que pertenecen, asi como tambien la fecha desde y la fecha hasta del
    'correlativo del empleado.
    
'    SQL = "SELECT CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta " _
'        & "FROM  (SELECT  CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
'        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
'        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta " _
'        & "FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB2.Nomina2, TB2.UltimaCorrida " _
'        & "FROM (SELECT TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1,  " _
'        & "MIN(MA_CorrelativosMarcajes.fDesde) AS FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta " _
'        & "FROM TR_IMPORTAMARCAJE INNER JOIN " _
'        & "MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN " _
'        & "MA_CorrelativosMarcajes ON MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar " _
'        & "WHERE (TR_IMPORTAMARCAJE.migrado = 1) " _
'        & "GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta)  " _
'        & "AS TB1 LEFT OUTER JOIN " _
'        & "(SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaFinPeriodo) AS UltimaCorrida " _
'        & "FROM MA_NOMINA_PROCESADAS " _
'        & "WHERE (cu_estatus = 'NOMINA') " _
'        & "GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T "
'

            'se eliminio el where (TR_IMPORTAMARCAJE.migrado = 1)
            'implica que para evaluar inasistencia la persona necesitaba no solo tener un registro en tr_importamarcaje
            'si no que este registro haya sido migrado tambien
            
            
    sql = "SELECT CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta " _
        & "FROM  (SELECT  CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta " _
        & "FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB2.Nomina2, TB2.UltimaCorrida " _
        & "FROM (SELECT TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1,  " _
        & "MIN(MA_CorrelativosMarcajes.fDesde) AS FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta " _
        & "FROM TR_IMPORTAMARCAJE INNER JOIN " _
        & "MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN " _
        & "MA_CorrelativosMarcajes ON MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar " _
        & "GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta)  " _
        & "AS TB1 LEFT OUTER JOIN " _
        & "(SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaCorrida) AS UltimaCorrida " _
        & "FROM MA_NOMINA_PROCESADAS " _
        & "WHERE (cu_estatus = 'NOMINA') " _
        & "GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T "
        


        '& "WHERE (FechaDesde < FechaHasta) " '_
        '& "ORDER BY CodigoEmpleado, FechaDesde "

    Debug.Print sql
        
    Conexion.CommandTimeout = 0
    Set rs = Conexion.Execute(sql)

    If Not rs.BOF Then
        rs.MoveFirst
    End If
    
    numempleados = 0
    frm_AgenteMigradorMarcajes2.Show
    
    'Aqui se trae todos los dias libres que hay en el periodo de tiempo que se va a evaluar, es decir la minina fecha desde de los empleados y
    'la maxima fecha hasta.
    
    sql = "SELECT CAST(Feriado AS DATETIME) As Feriado FROM (SELECT MIN(FechaDesde) As FechaDesde, MAX(FechaHasta) As FechaHasta " _
        & "FROM (SELECT  CodigoEmpleado, Nomina, FechaDesde, FechaHasta FROM (SELECT CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2  " _
        & "IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME) > CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME)  " _
        & "ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde, CAST(ISNULL(FechaHasta, GETDATE()) AS DATETIME) AS FechaHasta FROM  " _
        & "(SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB2.Nomina2, TB2.UltimaCorrida FROM (SELECT  " _
        & "TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1, MIN(MA_CorrelativosMarcajes.fDesde)  " _
        & "AS FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta FROM TR_IMPORTAMARCAJE INNER JOIN MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple =  " _
        & "MA_PERSONAL.cu_codigo INNER JOIN MA_CorrelativosMarcajes ON MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar WHERE  " _
        & "(TR_IMPORTAMARCAJE.migrado = 1) GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta)  " _
        & "AS TB1 LEFT OUTER JOIN (SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaCorrida) AS UltimaCorrida FROM MA_NOMINA_PROCESADAS " _
        & "WHERE (cu_estatus = 'NOMINA') GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T WHERE  " _
        & "(FechaDesde < FechaHasta)) As TB) AS TBFechas, (SELECT CAST(FECHAFERIADO + '/' + CAST(YEAR(GETDATE()) AS nvarchar(4)) AS datetime)  " _
        & "AS Feriado FROM MA_FERIADOS) AS TBFeriado "

        ' & " WHERE (Feriado BETWEEN TBFechas.FechaDesde AND TBFechas.FechaHasta)"

    Debug.Print sql
    
    If (rs!CodigoEmpleado = "17953956") Then 'Debugging...
        Debug.Print sql
    End If
    
    Set RsFeriados = Conexion.Execute(sql)
        
    While Not rs.EOF
        Debug.Print rs.RecordCount
    Rss.CursorLocation = adUseServer
    If Rss.State = 1 Then Rss.Close
    Rss.Open "TR_ASISTENCIA", Conexion, adOpenStatic, adLockBatchOptimistic, adCmdTable
        
        numempleados = numempleados + 1
        frm_AgenteMigradorMarcajes2.Caption = "Evaluando Asistencias e Inasistencias..."
        
        'mVar = DevolverCortes(CStr(rs!Nomina), CDate(rs!FechaDesde), CDate(rs!FechaHasta))
        
        ' Esto es nuevo, simplemente es una funcion, para no traerme todas las inasistencias del periodo cada vez que se ejecute el agente
        ' si ya tengo horas extras aprobadas, o horas extras por aprobar en el periodo  no traigo las inasistencias anteriores a estas fechas
        ' no se puede manejar igual que con las horas por aprobar por que al grabar cambia el usuario, y se se elimina la inasistencia en el human
        ' ya no queda registro del cambio o de que existiera.
        
        fdesde = ObtenerUltimaEjecucion(rs!fechadesde, rs!CodigoEmpleado)
        'fdesde = mvar(0)
        

        
        fhasta = rs!FechaHasta

        If CDate(fdesde) > CDate(rs!FechaHasta) Then fdesde = fhasta - 1
        'fHasta = CDate(IIf(Rs!FechaHasta < Now, Rs!FechaHasta, DateAdd("d", -1, Now)))
        If G_Tope <> "" Then fhasta = G_Tope

        If G_CargaDiaria = 1 Then fdesde = fhasta
        If G_Desde <> "" Then fdesde = G_Desde

        numerodias = DateDiff("d", fdesde, fhasta) + 1
        frm_AgenteMigradorMarcajes2.ProgressBar.Max = CInt(numerodias)
        frm_AgenteMigradorMarcajes2.ProgressBar.Value = 1
        
        For i = CDate(CDate(fdesde)) To CDate(fhasta)
            
            Debug.Print rs.Source
            
            If frm_AgenteMigradorMarcajes2.ProgressBar.Value = CInt(numerodias) Then
                frm_AgenteMigradorMarcajes2.ProgressBar.Value = 1
            Else
                frm_AgenteMigradorMarcajes2.ProgressBar.Value = frm_AgenteMigradorMarcajes2.ProgressBar.Value + 1
            End If
            
            'Aqui se verifica si estaba de permiso, en reposo, suspendido o de vacaciones.
            
            sql = "SELECT cu_codigo AS Codigo FROM MA_CAMBIO_ESTATUS WHERE ((cu_codigo = '" & rs!CodigoEmpleado & "') AND (CONVERT(datetime, '" & Mid(i, 1, 10) & "', 103) " _
                & " BETWEEN fDesde AND fHasta) AND (cu_codEstatus IN ('SUSPENDIDO', 'VACACIONES', 'PERMISO', 'REPOSO'))) OR " _
                & "((cu_codigo = '" & rs!CodigoEmpleado & "') AND (fHasta = '') AND (fDesde <= Cast('" & Mid(i, 1, 10) & "' as Datetime)) AND " _
                & "(cu_codEstatus IN ('SUSPENDIDO', 'VACACIONES', 'PERMISO', 'REPOSO')))"
            
            Debug.Print sql
            
            If rs!CodigoEmpleado = "3928078" Or rs!CodigoEmpleado = "17564597" Then
                Debug.Print "Hola" 'Debugging...
            End If
            If rs!CodigoEmpleado = "21074498" Or rs!CodigoEmpleado = "15937038" Then
                Debug.Print "Hola" 'Debugging...
            End If
            
            'GoTo test
            'End If
            
            Set RsVacaciones = Conexion.Execute(sql)
            
            If RsVacaciones.EOF Then
            
                Select Case Weekday(i)
                
                    Case 1
                        Dia = "Domingo"
                    Case 2
                        Dia = "Lunes"
                    Case 3
                        Dia = "Martes"
                    Case 4
                        Dia = "Miercoles"
                    Case 5
                        Dia = "Jueves"
                    Case 6
                        Dia = "Viernes"
                    Case 7
                        Dia = "Sabado"
                        
                End Select
        
                Feriado = False
                
                If Not RsFeriados.EOF Then
                    While Not RsFeriados.EOF
                        If CDate(RsFeriados!Feriado) = i Then
                            Feriado = True
                        End If
                        RsFeriados.MoveNext
                    Wend
                    RsFeriados.MoveFirst
                End If
                                                                  
                ' ** CUESTI�N DE D�AS LIBRES ** '

                If MultiplesDiasLibres Then

                    'Esta consulta se utiliza para determinar si tiene marcaje este dia, cual es el dia libre del empleado, cuantos marcajes tiene este dia
                    'y a que n�mina pertenece.

                    sql = "SELECT Tb1.bas_horass, Tb2.*, Tb3.Marcajes, Tb4.cu_codnomina, " _
                        & "Tb4.cu_tipo_horario FROM (SELECT bas_horass " _
                        & "From baseproc " _
                        & "WHERE (bas_claves = '" & rs!CodigoEmpleado & "') AND (bas_fechas = '" & Mid(i, 1, 10) & "') ) AS Tb1 CROSS JOIN " _
                        & "(SELECT bu_Lunes, bu_Martes, bu_Miercoles, bu_Jueves, bu_Viernes, " _
                        & "bu_Sabado, bu_Domingo From MA_PERSONAL_DIASLIBRES WHERE " _
                        & "(cu_CodPersonal = '" & rs!CodigoEmpleado & "')) AS Tb2 CROSS JOIN " _
                        & "(SELECT COUNT(*) AS Marcajes " _
                        & "FROM baseproc AS baseproc_1 " _
                        & "WHERE (bas_claves = '" & rs!CodigoEmpleado & "') AND (bas_fechas = '" & Mid(i, 1, 10) & "')) AS Tb3 CROSS JOIN " _
                        & "(SELECT CAST(cu_codnomina AS nvarchar) AS cu_codnomina, " _
                        & "cu_tipo_horario FROM MA_PERSONAL AS MA_PERSONAL_1 " _
                        & "WHERE (cu_codigo = '" & rs!CodigoEmpleado & "')) AS Tb4"

                    Debug.Print sql

                    Set mRs = Conexion.Execute(sql)

                    If Not mRs.EOF Then

                        DiaLibre = False

                        If mRs!cu_tipo_horario = "F" Then

                            Select Case Weekday(CDate(i))

                                Case 2
                                    DiaLibre = CBool(mRs!bu_Lunes)
                                Case 3
                                    DiaLibre = CBool(mRs!bu_Martes)
                                Case 4
                                    DiaLibre = CBool(mRs!bu_Miercoles)
                                Case 5
                                    DiaLibre = CBool(mRs!bu_Jueves)
                                Case 6
                                    DiaLibre = CBool(mRs!bu_Viernes)
                                Case 7
                                    DiaLibre = CBool(mRs!bu_Sabado)
                                Case 1
                                    DiaLibre = CBool(mRs!bu_Domingo)

                            End Select

                        ElseIf mRs!cu_tipo_horario = "R" Then

                            sql = "select COUNT(*) esLibre from TR_DIASLIBRES_ROTATIVO " _
                            & "WHERE du_fechaAsistencia = '" & i & "' and cu_codpersonal " _
                            & " = '" & rs!CodigoEmpleado & "' and nu_valor = '1'"

                            Set RsDiasLibresRotativo = Conexion.Execute(sql)

                             If RsDiasLibresRotativo!esLibre > 0 Then
                                DiaLibre = True
                                RsDiasLibresRotativo.Close
                             End If

                        End If
                    
                    End If

                Else
                
                    'Esta consulta se utiliza para determinar si tiene marcaje este dia, cual es el dia libre del empleado, cuantos marcajes tiene este dia
                    'y a que n�mina pertenece.
                    
                    sql = "SELECT Tb1.bas_fechas, Tb2.cu_DiaLibre, Tb3.Marcajes, Tb4.cu_codnomina " _
                        & "FROM (SELECT bas_fechas " _
                        & "From baseproc " _
                        & "WHERE (bas_claves = '" & rs!CodigoEmpleado & "') AND (bas_fechas = '" & Mid(i, 1, 10) & "') ) AS Tb1 CROSS JOIN " _
                        & "(SELECT cu_DiaLibre " _
                        & "From MA_PERSONAL " _
                        & "WHERE (cu_codigo = '" & rs!CodigoEmpleado & "')) AS Tb2 CROSS JOIN " _
                        & "(SELECT COUNT(*) AS Marcajes " _
                        & "FROM baseproc AS baseproc_1 " _
                        & "WHERE (bas_claves = '" & rs!CodigoEmpleado & "') AND (bas_fechas = '" & Mid(i, 1, 10) & "')) AS Tb3 CROSS JOIN " _
                        & "(SELECT CAST(cu_codnomina AS nvarchar) AS cu_codnomina " _
                        & "FROM MA_PERSONAL AS MA_PERSONAL_1 " _
                        & "WHERE (cu_codigo = '" & rs!CodigoEmpleado & "')) AS Tb4"
    
                    Debug.Print sql
                    
                    Set mRs = Conexion.Execute(sql)
                    
                    If Not mRs.EOF Then
    
                        DiaLibre = False
                        
                        Select Case mRs!cu_DiaLibre
                             
                            Case "Lunes"
                                If Weekday(CDate(i)) = 2 Then
                                    DiaLibre = True
                                End If
                            Case "Martes"
                                If Weekday(CDate(i)) = 3 Then
                                    DiaLibre = True
                                End If
                            Case "Mi�coles"
                                If Weekday(CDate(i)) = 4 Then
                                    DiaLibre = True
                                End If
                            Case "Jueves"
                                If Weekday(CDate(i)) = 5 Then
                                    DiaLibre = True
                                End If
                            Case "Viernes"
                                If Weekday(CDate(i)) = 6 Then
                                    DiaLibre = True
                                End If
                            Case "S�bado"
                                If Weekday(CDate(i)) = 7 Then
                                    DiaLibre = True
                                End If
                            Case "Domingo"
                                If Weekday(CDate(i)) = 1 Then
                                    DiaLibre = True
                                End If
                            Case "Fin de Semana"
                                If Weekday(CDate(i)) = 1 Or Weekday(CDate(i)) = 7 Then
                                    DiaLibre = True
                                End If
                            Case Else
    
                            sql = "select COUNT(*) esLibre from TR_DIASLIBRES_ROTATIVO  where du_fechaAsistencia = '" & i & "' and cu_codpersonal = '" & rs!CodigoEmpleado & "' and nu_valor = '1'"
                            Set RsDiasLibresRotativo = Conexion.Execute(sql)
                            
                             If RsDiasLibresRotativo!esLibre > 0 Then
                                DiaLibre = True
                                RsDiasLibresRotativo.Close
                             End If
                            
                        End Select
                    
                    End If
                
                End If

            If Not mRs.EOF Then
                If mRs!Marcajes < 2 Then
                    If Not Feriado Then
                        If Not DiaLibre Then
                            sql = "select * from TR_ASISTENCIA where cu_codpersonal = '" & rs!CodigoEmpleado & "' and du_fechaAsistencia = '" & i & "'"
                            Debug.Print sql
                            Set RsAsistencia = Conexion.Execute(sql)
                            If RsAsistencia.EOF Then
                                'Se utilizo el UpdateBatch para poder enviar los datos en lotes y asi
                                'reducir las conexiones dentro del ciclo for
                                Rss.AddNew
                                Rss("cu_codnomina") = mRs!cu_codnomina
                                Rss("cu_codpersonal") = rs!CodigoEmpleado
                                Rss("du_fechaAsistencia") = Mid(i, 1, 10)
                                Rss("cu_codAsistenciatipo") = "0000000002"
                                Rss("cu_codusuario") = "AUTO"
                                Rss("ds_fecha_modificacion") = Mid(Now(), 1, 10)
                                Rss("nu_valor") = 1
                            End If
                        End If
                    End If
                End If
            End If
           End If
        Next i
        RsVacaciones.Close
        Rss.UpdateBatch
                
nxt:
                
        rs.MoveNext
        Rss.Close

    Wend
    
    rs.Close
    
End Sub

Public Function Recalcular_FormatoHoras(ByVal pHoras As Double) As Double

    On Error GoTo err
    
    Dim A, B, C, D, E, F, G
    
    Dim Valor As Double
    
    If G_FormatoHorasconMinutos Then
        
        A = Fix(pHoras)
        
        B = pHoras
        
        C = pHoras - A
        
        D = C * 60
        
        E = CInt(FormatNumber(D, 2, vbTrue, vbFalse, vbFalse))
        
        F = E / 100
        
        G = A + F
        
        Valor = G
             
    Else
        Valor = pHoras
    End If
    
    Recalcular_FormatoHoras = Valor
    
    Exit Function
    
err:

    Recalcular_FormatoHoras = pHoras

End Function

Public Sub CargarHorasExtrasYFaltantes()

    Dim DiaLibre                                As Boolean
    Dim rs                                           As New ADODB.Recordset
    Dim Rs2                                          As New ADODB.Recordset
    Dim Rs3                                         As New ADODB.Recordset
    Dim Rss                                         As New ADODB.Recordset
    Dim RsFeriados                                  As New ADODB.Recordset
    Dim CantidadRegistros                           As Integer
    Dim HorasExtrasDiurnas                          As Double
    Dim HorasExtrasNocturnas                        As Double
    Dim HorasNocturnas                              As Double
    Dim HorasFaltantes                              As Double
    Dim DiaDescanso                                 As Boolean
    Dim Feriado                                     As Boolean
    Dim HorasTrabajadas                             As Double
    Dim HorasExtrasPosTurno                         As Double
    Dim HorasExtrasPreTurno                         As Double
    Dim TempMonto                                   As Double
    Dim test                                        As Double
    
    'Dim Gracia As Date
    
    'Gracia = Format("00:00", "mm:ss")
    
    'Variable para minutos de gracia.
    'Gracia = DateAdd("n", G_Gracia, Gracia)
    
'    SQL = "SELECT CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta " _
'        & "FROM  (SELECT  CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
'        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
'        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta " _
'        & "FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB2.Nomina2, TB2.UltimaCorrida " _
'        & "FROM (SELECT TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1,  " _
'        & "MIN(MA_CorrelativosMarcajes.fDesde) AS FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta " _
'        & "FROM TR_IMPORTAMARCAJE INNER JOIN " _
'        & "MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN " _
'        & "MA_CorrelativosMarcajes ON MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar " _
'        & "WHERE (TR_IMPORTAMARCAJE.migrado = 1) " _
'        & "GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta)  " _
'        & "AS TB1 LEFT OUTER JOIN " _
'        & "(SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaFinPeriodo) AS UltimaCorrida " _
'        & "FROM MA_NOMINA_PROCESADAS " _
'        & "WHERE (cu_estatus = 'NOMINA') " _
'        & "GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T "
'
'        '& "WHERE (FechaDesde < FechaHasta) " '_
'        '& "ORDER BY CodigoEmpleado, FechaDesde "

    sql = "SELECT CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta " _
        & "FROM  (SELECT  CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta " _
        & "FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB2.Nomina2, TB2.UltimaCorrida " _
        & "FROM (SELECT TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1,  " _
        & "MIN(MA_CorrelativosMarcajes.fDesde) AS FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta " _
        & "FROM TR_IMPORTAMARCAJE INNER JOIN " _
        & "MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN " _
        & "MA_CorrelativosMarcajes ON MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar " _
        & "GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta)  " _
        & "AS TB1 LEFT OUTER JOIN " _
        & "(SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaCorrida) AS UltimaCorrida " _
        & "FROM MA_NOMINA_PROCESADAS " _
        & "WHERE (cu_estatus = 'NOMINA') " _
        & "GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T "
                
        '& "WHERE (FechaDesde < FechaHasta) " '_
        '& "ORDER BY CodigoEmpleado, FechaDesde "
       
    Debug.Print sql
    
    Set rs = Conexion.Execute(sql)
    Conexion.CommandTimeout = 0
    Rss.CursorLocation = adUseServer
    
'    If Rss.State = 1 Then Rss.Close
'    Rss.Open "TR_NOMINA_HORASEXTRAS_POR_APROBAR", Conexion, adOpenStatic, adLockBatchOptimistic, adCmdTable
    
    If Not rs.BOF Then
        rs.MoveFirst
    End If
    
    numempleados = 0
    frm_AgenteMigradorMarcajes2.Show
    
    While Not rs.EOF
        numempleados = numempleados + 1
        frm_AgenteMigradorMarcajes2.Caption = "Evaluando las Horas Extras y Faltantes..."
        
        mVar = DevolverCortes(CStr(rs!Nomina), CDate(rs!fechadesde), CDate(rs!FechaHasta))
        fdesde = mVar(0)
    
        'hay que poner una validacion aca para que tome las cedulas :D
        fdesde = ObtenerUltimaEjecucion(mVar(0), rs!CodigoEmpleado)
        fhasta = CDate(rs!FechaHasta)
        
        
        'fdesde = "18/11/2014"
        
        'fDesde = CDate(Rs!FechaDesde)
        'fHasta = CDate(Rs!FechaHasta)
        'fHasta = CDate(IIf(Rs!FechaHasta < Now, Rs!FechaHasta, DateAdd("d", -1, Now)))
        
        If CDate(fdesde) > CDate(fhasta) Then fdesde = fhasta - 1
               
        If G_Tope <> "" Then fhasta = G_Tope
        If G_Desde <> "" Then fdesde = G_Desde
        If G_CargaDiaria = 1 Then fdesde = fhasta
        
        numerodias = DateDiff("d", fdesde, fhasta) + 1
        frm_AgenteMigradorMarcajes2.ProgressBar.Max = CInt(numerodias)
        frm_AgenteMigradorMarcajes2.ProgressBar.Value = 1
'jesus

        Set Rs3 = Nothing
        
        sql = "SELECT bas_horass AS Hora, bas_clasificacion AS Clasificacion FROM baseproc WHERE bas_claves = '" & rs!CodigoEmpleado & "' AND bas_fechas BETWEEN '" & fdesde & "' AND '" & Replace(CStr(fhasta), ".", "") & "' AND bas_clasificacion <> 'INASISTENCIA'  order by bas_horass"
        Debug.Print sql
        Set Rs3 = Conexion.Execute(sql)
        
        If rs!CodigoEmpleado = "29" Then
            Debug.Print "stop"
        End If
               
               
        For i = CDate(fdesde) To CDate(fhasta)
            
            If frm_AgenteMigradorMarcajes2.ProgressBar.Value = CInt(numerodias) Then
                frm_AgenteMigradorMarcajes2.ProgressBar.Value = 1
            Else
                frm_AgenteMigradorMarcajes2.ProgressBar.Value = frm_AgenteMigradorMarcajes2.ProgressBar.Value + 1
            End If
            
            'If Rss.State = 1 Then Rss.Close
            
            'If Rss.State = 0 Then Rss.Open "TR_NOMINA_HORASEXTRAS_POR_APROBAR", Conexion, adOpenStatic, adLockBatchOptimistic, adCmdTable
            
            Feriado = False
            DiaDescanso = False
                        
            Select Case Weekday(i)
                Case 1
                    Dia = "Domingo"
                Case 2
                    Dia = "Lunes"
                Case 3
                    Dia = "Martes"
                Case 4
                    Dia = "Miercoles"
                Case 5
                    Dia = "Jueves"
                Case 6
                    Dia = "Viernes"
                Case 7
                    Dia = "Sabado"
            End Select
                                    
            'Sql = "EXECUTE DATOS_BIOMETRICO '" & Rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "', '" & Mid(i, 1, 5) & "', '" & Dia & "';"
            If rs!CodigoEmpleado = 11607640 Then
                Debug.Print "HOLA"
            End If
            
            
            Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' and aprobada = '0'")
            
            sql = "EXECUTE DATOS_BIOMETRICO '" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "', '" & Mid(i, 1, 5) & "', '" & Dia & "'" & IIf(MultiplesDiasLibres, ",1", ",0") & ";"
            Debug.Print sql
            
            If rs!CodigoEmpleado = "29" Then 'Debugging ...
                Debug.Print "Hola"
            End If

            Set Rs2 = Conexion.Execute(sql)
            
            HorasHorario = 0
            
            HayDescanso = False
            
            If Not Rs2.EOF Then
'***************************************************************************************************************************************************
                    HayDescanso = False
                    Rs2.Fields.Refresh
'Esto de aqui para calcular las horas trabajas y las horas totales segun su horario
                    For k = 0 To Rs2.Fields.Count - 1
                      If UCase(Rs2.Fields(k).Name) = UCase("FinDescanso") Then
                        HayDescanso = True
                        Exit For
                      'Debug.Print sql
                      End If
                      
                      'Debug.Print rs2.fields(k).Name
                      'Debug.Print rs2.fields(k)
                    Next k
                            
                    Dim Minutos_Gracia As Integer
                    
                    Minutos_Gracia = CInt(DateDiff("n", CDate(Rs2!horaEntrada), CDate(Rs2!HoraGracia)))
                    Minutos_Gracia = IIf(Minutos_Gracia < 0, 0, Minutos_Gracia)
                    
                    'Debug.Print SQL
                    
                    HorasHorario = Round(DateDiff("n", Rs2!horaEntrada _
                    , Rs2!InicioDescanso) / 60, 2) + Round(DateDiff("n", Rs2!FinDescanso _
                    , Rs2!HoraSalida) / 60, 2)
                    
                    'Gracia = DateAdd("n", Minutos_Gracia, Gracia)
                                                        
                    'Se cuentan las horas trabajadas para calcularlas con las horas del horario.
                    If Not Rs3.EOF Then
                        HorasTrabajadas = 0
                        
                        Rs3.MoveFirst
                        While Not Rs3.EOF
                       'HorasTrabajadas = 0

                            If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
                                primerahora = CDate(Rs3!Hora)
                                Rs3.MoveNext
'                                    test = Round((DateDiff("n", primerahora, Rs3!hora) / 60), 2)
'                                    Debug.Print test
                                If Not Rs3.EOF Then
                                    HorasTrabajadas = HorasTrabajadas + Round(( _
                                    DateDiff("n", primerahora, Rs3!Hora) / 60), 2)
                                End If
                            End If
                            If Not Rs3.EOF Then Rs3.MoveNext
                        Wend
                        Rs3.MoveFirst
                    End If
                
'***************************************************************************************************************************************************
                    Monto = 0
                
                    ' Calculo de Dia-Domingo-Trabajado y Horas-Domingo-Trabajadas
                    If Dia = "Domingo" Then
                
                    ' ** Buscar y extraer informaci�n de Marcajes ** '
                    
                        If Not Rs3.EOF Then
                            While Not Rs3.EOF
                            
                                If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
                                    primerahora = CDate(Rs3!Hora)  'Se extrae el primer marcaje
                                    Rs3.MoveNext
                                    If Not Rs3.EOF Then
                                    'Debug.Print dia
                                        Calculado = Round((DateDiff("n", primerahora, Rs3!Hora) / 60), 2)
                                            'If calculado > 15 Then
                                            'Debug.Print calculado
                                            'End If
                                        Monto = Monto + IIf(Calculado <= 0, 0, Calculado)
                                    End If
                                End If
                                If Not Rs3.EOF Then Rs3.MoveNext
                            Wend
                            Rs3.MoveFirst
                        End If
                        
                        ' ** Fin Busqueda ** '
                        
                        ' Borrar registros anteriores (Si hubieren).
                        'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.T.D' and aprobada = '0'")
                        'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'D.T.D' and aprobada = '0'")
                    
                        If Monto >= G_HorasDiaDomingoT Then ' Convertir el 6 en una variable (Cantidad de horas a ser consideradas como Domingo Trabajado)
                           
                            If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'D.T.D' and aprobada = '1'").EOF Then
                                'Rss.AddNew
                                'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                'Rss.Fields("cu_tipohora") = "D.T.D"
                                'Rss.Fields("nu_monto") = 1
                                Tipo = "D.T.D"
                                MontoInsert = 1
                                Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                            End If
                           
                           'If Monto > 8 Then 'Borrar este IF... no tiene sentido
                                                                     
                            If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.T.D' and aprobada = '1'").EOF Then
                                'Rss.AddNew
                                'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                'Rss.Fields("cu_tipohora") = "H.T.D"
                                TempMonto = Recalcular_FormatoHoras(Monto)
                                'Rss.Fields("nu_monto") = FormatNumber(IIf(G_Excedente = 1, TempMonto - G_HorasDiaDomingoT, TempMonto), 2)
                                Tipo = "H.T.D"
                                MontoInsert = FormatNumber(IIf(G_Excedente = 1, TempMonto - G_HorasDiaDomingoT, TempMonto), 2)
                                Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                            End If
                                
                           'End If ' Borrar
                        
                        Else ' Borrar todo el Else ya que no tiene sentido... las horas se deben grabar obligatoriamente.
                            If Monto > 0 Then
                                If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.T.D' and aprobada = '1'").EOF Then
                                    'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                    'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                    'Rss.Fields("cu_tipohora") = "H.T.D"
                                    TempMonto = Recalcular_FormatoHoras(Monto)
                                    'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                    Tipo = "H.T.D"
                                    MontoInsert = FormatNumber(TempMonto, 2)
                                    Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                End If
                            End If
                        End If
                    
                    End If
            
                    Monto = 0
                
                    'Se verifica si las horas no fueron cumplidas en d�a de descanso.
                    
                    If Rs2!DiaLibre <> 0 Then
                        
                        If Rs2!HDT = 0 Then
                            ' ** Buscar y extraer informaci�n de Marcajes ** '
                            If Not Rs3.EOF Then
                                While Not Rs3.EOF
                                    If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
                                        primerahora = CDate(Rs3!Hora)  'Se extrae el primer marcaje
                                        Rs3.MoveNext
                                        If Not Rs3.EOF Then
                                        'Debug.Print dia
                                            Calculado = Round((DateDiff("n", primerahora, Rs3!Hora) / 60), 2)
                                            
                                            Monto = Monto + IIf(Calculado <= 0, 0, Calculado)
                                        End If
                                    End If
                                    If Not Rs3.EOF Then Rs3.MoveNext
                                Wend
                                Rs3.MoveFirst
                            End If
                            'Fin Busqueda
                            
                            ' Borrar registros anteriores si los hubiere
                            'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.D.T' and aprobada = '0'")
                            'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'D.D.T' and aprobada = '0'")
                        
                            Monto = FormatNumber(Monto, 2)
                            
                            If Monto >= G_HorasDiaLibreT Then
                            
                               If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'D.D.T' and aprobada = '1'").EOF Then
                                    'Rss.AddNew
                                    'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                    'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                    'Rss.Fields("cu_tipohora") = "D.D.T"
                                    'Rss.Fields("nu_monto") = 1
                                    Tipo = "D.D.T"
                                    MontoInsert = 1
                                    Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                               End If
                                
                               'If Monto > 8 Then
                                If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.D.T' and aprobada = '1'").EOF Then
                                    
                                    'Rss.AddNew
                                    'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                    'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                    'Rss.Fields("cu_tipohora") = "H.D.T"
                                    TempMonto = Recalcular_FormatoHoras(Monto)
                                    'Rss.Fields("nu_monto") = FormatNumber(IIf(G_Excedente = 1, TempMonto - G_HorasDiaLibreT, TempMonto), 2)
                                    Tipo = "H.D.T"
                                    MontoInsert = FormatNumber(IIf(G_Excedente = 1, TempMonto - G_HorasDiaLibreT, TempMonto), 2)
                                    Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                End If
                               'End If
                            
                            Else
                        
                                If Monto > 0 Then
                                
                                    If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.D.T' and aprobada = '1'").EOF Then
                                        'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                        'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                        'Rss.Fields("cu_tipohora") = "H.D.T"
                                        TempMonto = Recalcular_FormatoHoras(Monto)
                                        'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                        Tipo = "H.D.T"
                                        MontoInsert = FormatNumber(TempMonto, 2)
                                        Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                    End If
                                    
                                End If
                                
                            End If
                            
                        End If
                        
                    End If
                
                    Monto = 0
                    
                    'Se verifica si las horas no fueron cumplidas en d�a feriado
                    
                    If Rs2!Feriado <> 0 Then
                        If Rs2!HFT = 0 Then
                            '************
                            If Not Rs3.EOF Then
                                While Not Rs3.EOF
                                    If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
                                        primerahora = CDate(Rs3!Hora)  'Se extrae el primer marcaje
                                        Rs3.MoveNext
                                        If Not Rs3.EOF Then
                                         Calculado = Round((DateDiff("n", primerahora, Rs3!Hora) / 60), 2)
                                         Monto = Monto + IIf(Calculado <= 0, 0, Calculado)
                                        End If
                                    End If
                                    If Not Rs3.EOF Then Rs3.MoveNext
                                Wend
                                Rs3.MoveFirst
                            End If
                            '************
                            
                            'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F.T' and aprobada = '0'")
                            Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'D.F.T' and aprobada = '0'")
                           
                            Monto = FormatNumber(Monto, 2)
                        
                            If Monto >= G_HorasDiaFeriadoT Then
                                
                                If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'D.F.T' and aprobada = '1'").EOF Then
                                    'Rss.AddNew
                                    'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                    'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                    'Rss.Fields("cu_tipohora") = "D.F.T"
                                    'Rss.Fields("nu_monto") = 1
                                    Tipo = "D.F.T"
                                    MontoInsert = 1
                                    Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                End If
                                
                           'If Monto > 8 Then
                                If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F.T' and aprobada = '1'").EOF Then
                                    'Rss.AddNew
                                    'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                    'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                    'Rss.Fields("cu_tipohora") = "H.F.T"
                                    TempMonto = Recalcular_FormatoHoras(Monto)
                                    'Rss.Fields("nu_monto") = FormatNumber(IIf(G_Excedente = 1, TempMonto - G_HorasDiaFeriadoT, TempMonto), 2)
                                    Tipo = "H.F.T"
                                    MontoInsert = FormatNumber(IIf(G_Excedente = 1, TempMonto - G_HorasDiaFeriadoT, TempMonto), 2)
                                    Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                End If
                           'End If
                        
                            Else
                        
                                If Monto > 0 Then
                                    If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F.T' and aprobada = '1'").EOF Then
                                        'Rss.AddNew
                                        'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                        'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                        'Rss.Fields("cu_tipohora") = "H.F.T"
                                        TempMonto = Recalcular_FormatoHoras(Monto)
                                        'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                        Tipo = "H.F.T"
                                        MontoInsert = FormatNumber(TempMonto, 2)
                                        Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                    End If
                                End If
                            
                            End If
                            
    '                        Rss.AddNew
    '                        Rss.fields("cu_codpersonal") = rs!CodigoEmpleado
    '                        Rss.fields("du_fechacorte") = Mid(i, 1, 10)
    '                        Rss.fields("cu_tipohora") = "H.F.T"
    '                        Rss.fields("nu_monto") = monto
                        End If
                        
                    End If
                
                    HorasFaltantes = 0
                    horasJornadasNocturnas = 0
'                If hayDescanso Then
'                 If CDate(Format(rs2!InicioDescanso)) = CDate(Format(rs2!FinDescanso)) Then
'                    hayDescanso = False
'                 End If
'                End If
                    'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'J.N' and aprobada = '0'")
                    horasJornadasNocturnas = 0
                    
                    While Not Rs3.EOF
                    
                         If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
                             If Rs3!Clasificacion = "ENTRADA JORNADA" Then
        '                       Verificamos que la hora de entrada sea menor a las 5:00 am
                                 If Format(Rs3!Hora, "HH:mm:ss") < Format(G_FinJornadaNocturna, "HH:mm:ss") Then
        '                               Tomamos la diferencia de horas entre: si la hora del marcaje es mayor a la de la hora de entrada del horario entonces tomamos la hora del
        '                               marcaje para sacar la diferencia, es decir, la diferencia entre la hora del marcaje y las 5:00 am, sino entonces hacemos lo mismo pero con la
        '                               hora de entrada del horario.
                                        'horasJornadasNocturnas = Round(DateDiff("n", IIf(Format(rs3!hora, "HH:mm:ss") > Format(rs2!HoraEntrada, "HH:mm:ss"), Format(rs3!hora, "HH:mm:ss"), Format(rs2!HoraEntrada, "HH:mm:ss")), Format("05:00:00", "HH:mm:ss")) / 60, 2)
                                     horasJornadasNocturnas = Round(DateDiff("n", Format(Rs3!Hora _
                                     , "HH:mm:ss"), Format(G_FinJornadaNocturna, "HH:mm:ss")) / 60, 2)
                                 End If


                             End If
                         End If
                         
                         If Not Rs3.EOF Then Rs3.MoveNext
                    Wend
                   Debug.Print Rs3.RecordCount
                   Debug.Print rs!CodigoEmpleado
                    Rs3.MoveFirst
                    
                    While Not Rs3.EOF
                   
                        If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
                           If Rs3!Clasificacion = "SALIDA JORNADA" Then
                               If Format(Rs3!Hora, "HH:mm:ss") > Format(G_FinJornadaDiurna, "HH:mm:ss") Then
                                 horasJornadasNocturnas = horasJornadasNocturnas + Round( _
                                 DateDiff("n", Format(G_FinJornadaDiurna, "HH:mm:ss") _
                                 , Format(Rs3!Hora, "HH:mm:ss")) / 60, 2)
                               End If
                           End If
                        End If
                        If Not Rs3.EOF Then Rs3.MoveNext
                    
                    Wend
                                                                       
                    If Rs2!JN = 0 And horasJornadasNocturnas > 0 Then
                        If horasJornadasNocturnas > 0 Then
                           'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'J.N' and aprobada = '0'")
                           If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'J.N' and aprobada = '1'").EOF Then
                               'Rss.AddNew
                               'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                               'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                               'Rss.Fields("cu_tipohora") = "J.N"
                               TempMonto = Recalcular_FormatoHoras(horasJornadasNocturnas)
                               'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                               Tipo = "J.N"
                               MontoInsert = FormatNumber(TempMonto, 2)
                               sql = "Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")"
                               Debug.Print sql
                               Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                           End If
                        End If
                    End If
                
                    Rs3.MoveFirst
                                               
                    If Not CBool(Rs2!DiaLibre) And Not CBool(Rs2!Feriado) And HayDescanso Then
                        medioDiaEvaluado = False
                    
                    'On Error GoTo linea1
                    'Se verifica si solo se cumplio medio d�a
                    'GoTo linea1
                    'Se verifica si el marcaje de Salida fue antes de la de la entrada intermedia
                    'O si el marcaje de entrada mas los minutos de gracia fueron despues de la
                    'entrada intermedia mas los minutos de gracia
                    ' mas los minutos de Gracia lo cual indica que el empleado falto medio dia
                    
'                    If (CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(Rs2!Salida, "hh:mm:ss")) <= _
'                    CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate(Format( _
'                    DateAdd("n", G_Gracia, Rs2!InicioDescanso)))))) _
'                    Or _
'                    (CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
'                    DateAdd("n", G_Gracia, Format(Rs2!entrada, "hh:mm:ss"))))) >= _
'                    CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
'                    DateAdd("n", G_Gracia, Format(Rs2!FinDescanso)))))) Then

                     'Creo que en la condici�n anterior despues del OR estaba malo
                     'por que le sumo los minutos de gracia a los 2 valores, entonces
                     'que chiste tiene hacer eso (no tiene ningun efecto).
                     'La modifique para que si el marcaje de entrada fue despues de la hora de entrada
                     ' y fue antes de la misma mas el tiempo de gracia, entonces entra

                        If (CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(Rs2!Salida, "hh:mm:ss")) <= _
                        CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate(Format( _
                        Rs2!InicioDescanso))))) _
                        Or _
                        ((CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
                        Format(Rs2!Entrada, "hh:mm:ss"))))) >= _
                        CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
                        Format(Rs2!FinDescanso))))) Then
                     
                            If (CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(Rs2!Salida, "hh:mm:ss")) <= _
                            CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate(Format( _
                            DateAdd("n", Minutos_Gracia, Rs2!InicioDescanso)))))) _
                            Or _
                            ((CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
                            Format(Rs2!Entrada, "hh:mm:ss"))))) >= _
                            CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
                            Format(Rs2!FinDescanso))))) And _
                            (CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
                            Format(Rs2!Entrada, "hh:mm:ss"))))) <= _
                            (CDate(Format(Now(), "dd/mm/yyyy") & " " & CStr(CDate( _
                            DateAdd("n", Minutos_Gracia, Format(Rs2!FinDescanso)))))) Then
                     
                            'If Rs2!TuvoAsistencia = 0 Then
                                'If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "' AND cu_codusuario = 'AUTO' AND cu_codAsistenciatipo = '0000000002'").EOF Then
    '                            sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
    '                                "values('" & rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "',0.5)"
                                
                               ' sql = sql & "; INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                    "values('" & rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000001','AUTO', '" & Mid(Now(), 1, 10) & "'," & horasTrabajadas & ")"
                                'Conexion.Execute sql
                                'End If
                            'Else
                                'sql = "UPDATE TR_ASISTENCIA SET nu_valor = 0.5 WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "' AND cu_codusuario = 'AUTO'"
                                
                               ' If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "' AND cu_codusuario = 'AUTO' AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                    'sql = sql & "; INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                        "values('" & rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "',0.5)"
                                'Conexion.Execute sql
                                'End If
                            'End If
                        
                                If HorasTrabajadas < HorasHorario Then
    ''                        If CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!entrada, "hh:mm:ss")) < CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!InicioDescanso) - CDate(Format(rs2!Anticipacion, "hh:mm:ss"))) And CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!Salida, "hh:mm:ss")) <= CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!FinDescanso) + CDate(Format(rs2!Anticipacion, "hh:mm:ss"))) Then
    ''                            If CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!Salida, "hh:mm:ss")) < CDate(CStr(CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!InicioDescanso) - CDate(Format(rs2!Anticipacion, "hh:mm:ss"))))) Then
    ''                                HorasFaltantes = CInt(CDbl(CDate(CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!InicioDescanso) + CDate(Format(rs2!Anticipacion, "hh:mm:ss"))) - (CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!Salida, "hh:mm:ss"))))) * 48) / 2
    ''                            End If
    ''
    ''                            If CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!entrada, "hh:mm:ss")) > CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!HoraEntrada) + CDate(Format(rs2!Anticipacion, "hh:mm:ss"))) Then
    ''                                HorasFaltantes = HorasFaltantes + (CInt(CDbl(CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!entrada, "hh:mm:ss")) - (CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!HoraEntrada) + CDate(Format(rs2!Anticipacion, "hh:mm:ss"))))) * 48) / 2)
    ''                            End If
    ''
    ''                        Else
    ''                            If CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!Salida, "hh:mm:ss")) < CDate(CStr(CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!HoraSalida) - CDate(Format(rs2!Anticipacion, "hh:mm:ss"))))) Then
    ''                                HorasFaltantes = HorasFaltantes + (CInt(CDbl(CDate(CStr(CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!HoraSalida) - CDate(Format(rs2!Anticipacion, "hh:mm:ss"))))) - CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!Salida, "hh:mm:ss"))) * 48) / 2)
    ''                            End If
    ''
    ''                            If CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!entrada, "hh:mm:ss")) > CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!FinDescanso) + CDate(Format(rs2!Anticipacion, "hh:mm:ss"))) And (rs2!FinDescanso <> rs2!InicioDescanso) Then
    ''                                HorasFaltantes = HorasFaltantes + (CInt(CDbl(CDate(Format(Now(), "dd/mm/yyyy") & " " & Format(rs2!entrada, "hh:mm:ss")) - CDate(CDate(Format(Now(), "dd/mm/yyyy") & " " & rs2!FinDescanso) + CDate(Format(rs2!Anticipacion, "hh:mm:ss")))) * 48) / 2)
    ''                            End If
    ''
    ''                        End If
    
                                    HorasFaltantes = Abs(HorasTrabajadas - HorasHorario)
                                    Conexion.CommandTimeout = 0
                                    'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '0'")
                                    'HorasFaltantes = 0
                                    
                                    If HorasFaltantes > (G_MinutosSignificativos_HorasFaltantes / 60) Then
                                        If Rs2!HF = 0 Then
                                           If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '1'").EOF Then
                                                'Rss.AddNew
                                                'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                                'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                                'Rss.Fields("cu_tipohora") = "H.F"
                                                TempMonto = Recalcular_FormatoHoras(HorasFaltantes)
                                                'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                                medioDiaEvaluado = True
                                                Tipo = "H.F"
                                                MontoInsert = FormatNumber(TempMonto, 2)
                                                Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")

                                                'Aqu� la otra evaluaci�n.
                                                If HorasFaltantes > (G_MinutosSignificativos_Dia / 60) Then
                                                    If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 1 & ")"
                                                        Conexion.Execute sql
                                                    End If
                                                ElseIf HorasFaltantes > (G_MinutosSignificativos_Medio_Dia / 60) Then
                                                    If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 0.5 & ")"
                                                        Conexion.Execute sql
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                               
                                End If
                           
                            Else
                        
                               If HorasTrabajadas < HorasHorario Then
    
                                   HorasFaltantes = Abs(HorasTrabajadas - HorasHorario)
                                   Conexion.CommandTimeout = 0
                                   'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '0'")
                                   
                                   If HorasFaltantes > (G_MinutosSignificativos_HorasFaltantes / 60) Then
                                        If Rs2!HF = 0 Then
                                            If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '1'").EOF Then
                                                'Rss.AddNew
                                                'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                                'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                                'Rss.Fields("cu_tipohora") = "H.F"
                                                TempMonto = Recalcular_FormatoHoras(HorasFaltantes)
                                                'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                                medioDiaEvaluado = True
                                                Tipo = "H.F"
                                                MontoInsert = FormatNumber(TempMonto, 2)
                                                Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                                                                                                                           
                                                'Aqu� la otra evaluaci�n.
                                                If HorasFaltantes > (G_MinutosSignificativos_Dia / 60) Then
                                                    If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 1 & ")"
                                                        Conexion.Execute sql
                                                    End If
                                                ElseIf HorasFaltantes > (G_MinutosSignificativos_Medio_Dia / 60) Then
                                                    'Debug.Print "SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & Rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'"
                                                    
                                                    If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 0.5 & ")"
                                                        Conexion.Execute sql
                                                    End If
                                                End If
                                           End If
                                       End If
                                   End If
                                  
                               End If
                        
                               'Adicionalmente Agregar Inasistencia
                                
                               Select Case G_Accion_LlegadaTardia
                                
                                Case 0
                                
                                    'No se toma ninguna acci�n. Se deja a criterio del cliente
                                    'evaluar las horas faltantes y colocar inasistencia
                                    
                                Case 1, 2
                                    
                                    '1 Es para medio d�a y 2 el d�a completo, pero como la validaci�n
                                    'se refiere a si el empleado fallo el tiempo de gracia, y
                                    'se sobreentiende que esta trabajando solo medio d�a, entonces
                                    'se le graba inasistencia completa.
                                                          
                                    If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "' AND cu_codusuario = 'Auto' AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 1 & ")"
                                        Conexion.Execute sql
                                    End If
                                        
                               End Select
                     
                            End If
                           
                        End If
                        
                    End If
                
                    'Aqui terminan las verificaciones para cuando son medios d�as.

'***************************************************************************************************************************************************
                    
                    If Not CBool(Rs2!Feriado) And Not CBool(Rs2!DiaLibre) And Not medioDiaEvaluado Then
                        
                        HorasExtrasPosTurno = 0
                        HorasExtrasPreTurno = 0
                        
                        HorasExtrasDiurnas = 0
                        HorasExtrasNocturnas = 0
                        HorasFaltantes = 0
                        horasJornadasNocturnas = 0
                        
                        'horasHorario = Round(DateDiff("n", rs2!HoraEntrada, rs2!InicioDescanso) / 60, 2) + Round(DateDiff("n", rs2!FinDescanso, rs2!HoraSalida) / 60, 2)
                                              
                        ' WTF
                        'If horasHorario > 10 Then
                        
                        'horasHorario = horasHorario
                        'End If
                        ' END WTF
                        
'                        If (Format(rs2!HoraEntrada, "HH:mm:ss") > Format("19:00:00", "HH:mm:ss") Or Format(rs2!HoraEntrada, "HH:mm:ss") < Format("05:00:00", "HH:mm:ss")) Or (Format(rs2!HoraSalida, "HH:mm:ss") > Format("19:00:00", "HH:mm:ss") Or Format(rs2!HoraSalida, "HH:mm:ss") < Format("05:00:00", "HH:mm:ss")) Then
'                            JN = True
'                        End If
                        
                        'Se ingresan la cantidad de horas trabajadas a la tabla TR_ASISTENCIA
                        
                        'If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "' AND cu_codusuario = 'Auto' AND cu_codAsistenciatipo = '0000000002'").EOF Then
                        
                        'Else
                        
                        'End If
                        'Debug.Print sql
                        
                       ' Conexion.Execute sql
                                              
                        'Se verifica si el empleado no cumplio exactamente las horas que debia cumplir segun su horario.
                        'HorasTrabajadas>HorasHorario
                                                                       
                        If (Minutos_Gracia < 0) Or DateDiff("n", CDate(Rs2!horaEntrada), CDate(Rs2!Entrada)) > Minutos_Gracia Then
                        'If HorasTrabajadas <> HorasHorario Then
                        
                            If HorasTrabajadas > HorasHorario Then
                            
                                'Si es mayor quiere decir q hubo horas extras, por lo tanto hay q evaluar donde se produjeron esas horas
                                'para poder determinar si es HED u HEN.
                                'HorasExtrasDiurnas = 0
                                'HorasExtrasNocturnas = 0
                                
                                While Not Rs3.EOF
                                    If i = CDate(Mid(Rs3!Hora, 1, 10)) Then
'                                        primerahora = CDate(Rs3!hora)
'                                        Tipo = Rs3!clasificacion
'                                        Rs3.MoveNext
                                        
'                                        If Not Rs3.EOF Then
                                            

                                                                              
'                                            If (Format(primeraHora, "HH:mm:ss") < Format( _
'                                            DateAdd("n", Rs2!Anticipacion, Rs2!HoraEntrada), "HH:mm:ss") _
'                                            And tipo = "ENTRADA JORNADA") Then
'
'                                                'HorasExtrasDiurnas = HorasExtrasDiurnas + Round(DateDiff("n", Format(primeraHora, "HH:mm:ss"), IIf(Format(rs3!hora, "HH:mm:ss") > Format(rs2!HoraEntrada, "HH:mm:ss"), Format(rs2!HoraEntrada, "HH:mm:ss"), Format(rs3!hora, "HH:mm:ss"))) / 60, 2)
'                                                If Format(primeraHora, "HH:mm:ss") > "05:00:00" Then
'                                                    aux1 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                    , "05:00:00") / 60, 2)
'                                                    aux2 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                    , Format(Rs2!HoraEntrada, "HH:mm:ss")) / 60, 2)
'                                                    HorasExtrasDiurnas = HorasExtrasDiurnas + (aux2 - aux1)
'                                                Else
'
'                                                    If Format(Rs3!hora, "HH:mm:ss") < Rs2!HoraEntrada _
'                                                    And Rs3!Clasificacion = "SALIDA INTERMEDIA" Then
'                                                        aux1 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                        , "05:00:00") / 60, 2)
'                                                        aux2 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                        , Format(Rs3!hora, "HH:mm:ss")) / 60, 2)
'                                                        HorasExtrasDiurnas = HorasExtrasDiurnas + (aux2 - aux1)
'                                                    Else
'
'                                                        If Format(Rs3!hora, "hh:mm:ss") > Format(Rs2!HoraEntrada, "hh:mm:ss") Then
'                                                            aux1 = 0
'                                                            aux2 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                            , Format(Rs2!HoraEntrada, "HH:mm:ss")) / 60, 2)
'                                                        Else
'                                                            If Format(primeraHora, "HH:mm:ss") > "05:00:00" Then
'                                                                aux2 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                                , Format(Rs3!hora, "HH:mm:ss")) / 60, 2)
'                                                                aux1 = Round(DateDiff("n", Format(Rs3!hora, "HH:mm:ss") _
'                                                                , Format(Rs2!HoraEntrada, "HH:mm:ss")) / 60, 2)
'                                                            Else
'                                                                aux1 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                                , "05:00:00") / 60, 2)
'                                                                aux2 = Round(DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                                                , Format(Rs3!hora, "HH:mm:ss")) / 60, 2)
'                                                            End If
'                                                        End If
'
'                                                        HorasExtrasDiurnas = HorasExtrasDiurnas + (aux2 - aux1)
'
'                                                    End If
'
'                                                End If
'
'                                            End If
'
'                                        End If
'ojojesus
                                    If rs!CodigoEmpleado = 29 Or rs!CodigoEmpleado = 18005395 Then
                                            
                                        Debug.Print "revisar"
                                            
                                    End If
 'SE AGREGO 10/02/2015 ya que no calculaba horas extras diurnas si estas se hacian antes de la hora de
 'entrada de cada uno de los usuario por lo que se agrego un condicional que basicamente compara la diferencia
 'de hora entre la hora de entrada de la persona y la hora de entra que le corresponde segun su horario
 'para ver si tiene horas extras diurnas.
 
                                        If Rs3!Clasificacion = "ENTRADA JORNADA" Then
                                            If Abs(DateDiff("n", (Format(Rs3!Hora, "HH:mm:ss")), (Format(Rs2!horaEntrada, "HH:mm:ss"))) / 60) > 0 And Format(Rs3!Hora, "HH:mm:ss") < Format(G_FinJornadaDiurna, "HH:mm:ss") And Format(Rs3!Hora, "HH:mm:ss") > Format(G_FinJornadaNocturna, "HH:mm:ss") Then
                                                HorasExtrasDiurnas = Round(DateDiff("n", (Format(Rs3!Hora, "HH:mm:ss")), (Format(Rs2!horaEntrada, "HH:mm:ss"))) / 60, 2)
                                            End If
                                        End If

                                        If Rs3!Clasificacion = "SALIDA JORNADA" Then
                                        
                                            If Format(Rs3!Hora, "HH:mm:ss") > Format _
                                            (DateAdd("n", Rs2!Anticipacion, Rs2!HoraSalida), _
                                            "HH:mm:ss") Then
                                            
                                                If Format(Rs3!Hora, "HH:mm:ss") > _
                                                Format(G_FinJornadaDiurna, "HH:mm:ss") Then
                                                                                                   
                                                    HorasTemp = Round(DateDiff("n", Format _
                                                    (Rs2!HoraSalida, "HH:mm:ss") _
                                                    , Format(G_FinJornadaDiurna, "HH:mm:ss")) / 60, 2)

                                                    'G_FinJornadaDiurna
                                                    'Rs3!Hora
                                                    
                                                    HorasExtrasDiurnas = HorasExtrasDiurnas + _
                                                    IIf(HorasTemp > 0, HorasTemp, 0)
                                                    
                                                    HoraInicio = IIf(Format(Rs2!HoraSalida, "HH:mm:ss") _
                                                    > Format(G_FinJornadaDiurna, "HH:mm:ss"), _
                                                    Format(Rs2!HoraSalida, "HH:mm:ss"), _
                                                    Format(G_FinJornadaDiurna, "HH:mm:ss"))
                                                                                                       
                                                    HorasExtrasNocturnas = HorasExtrasNocturnas + _
                                                    Round(DateDiff("n", Format _
                                                    (HoraInicio, "HH:mm:ss") _
                                                    , Format(Rs3!Hora, "HH:mm:ss")) / 60, 2)
                                            
                                               Else
                                               
                                                    HorasExtrasDiurnas = HorasExtrasDiurnas + _
                                                    Round(DateDiff("n", Format _
                                                    (Rs2!HoraSalida, "HH:mm:ss") _
                                                    , Format(Rs3!Hora, "HH:mm:ss")) / 60, 2)
                                            
                                               End If
                                               
                                            End If
                                        Debug.Print sql
                                        End If
                                                                    
'                                        If (Format(Rs3!Hora, "HH:mm:ss") > Format( _
'                                        DateAdd("n", Rs2!Anticipacion, Rs2!HoraSalida), "HH:mm:ss")) _
'                                        And (Format(Rs3!Hora, "HH:mm:ss") < _
'                                        Format(G_FinJornadaDiurna, "HH:mm:ss") _
'                                        And Rs3!Clasificacion = "SALIDA JORNADA") Then
'
'                                             HorasExtrasDiurnas = HorasExtrasDiurnas + Round( _
'                                             DateDiff("n", Format(Rs2!HoraSalida, "HH:mm:ss") _
'                                             , Format(Rs3!Hora, "HH:mm:ss")) / 60, 2)
'
'                                        End If
                                        
'                                        If (Format(primeraHora, "HH:mm:ss") < _
'                                        Format(G_FinJornadaNocturna, "HH:mm:ss")) _
'                                        And (Format(primeraHora, "HH:mm:ss") < Format( _
'                                        DateAdd("n", Rs2!Anticipacion, Rs2!HoraEntrada), "HH:mm:ss")) Then
'
'                                            HorasExtrasNocturnas = HorasExtrasNocturnas + Round( _
'                                            DateDiff("n", Format(primeraHora, "HH:mm:ss") _
'                                            , Format(G_FinJornadaNocturna, "HH:mm:ss")) / 60, 2)
'
'                                        End If
'
'                                        If (Format(Rs3!Hora, "HH:mm:ss") > Format( _
'                                        DateAdd("n", Rs2!Anticipacion, Rs2!HoraSalida), "HH:mm:ss")) _
'                                        And (Format(Rs3!Hora, "HH:mm:ss") > _
'                                        Format(G_FinJornadaDiurna, "HH:mm:ss")) _
'                                        And Rs3!Clasificacion = "SALIDA JORNADA" Then
'
'                                            HorasExtrasNocturnas = HorasExtrasNocturnas + Round( _
'                                            DateDiff("n", Format(Rs2!HoraSalida, "hh:mm:ss") _
'                                            , Format(Rs3!Hora, "hh:mm:ss")) / 60, 2)
'
'                                        End If
                                        
                                    End If
                                    
                                    Rs3.MoveNext
                                
                                Wend
                                Rs3.MoveFirst

                                If Rs2!Hed = 0 And HorasExtrasDiurnas >= (G_TiempoConsideraHoraExtra / 60) Then
                                    'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & Rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.E.D' and aprobada = '0'")
                                    If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.E.D' and aprobada = '1'").EOF Then
                                        'Rss.AddNew
                                        'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                        'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                        'Rss.Fields("cu_tipohora") = "H.E.D"
                                        TempMonto = Recalcular_FormatoHoras(HorasExtrasDiurnas)
                                        'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                        Tipo = "H.E.D"
                                        MontoInsert = FormatNumber(TempMonto, 2)
                                        Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                    End If
                                End If
        
                                If Rs2!Hen = 0 And HorasExtrasNocturnas >= (G_TiempoConsideraHoraExtra / 60) Then
                                    'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.E.N' and aprobada = '0'")
                                    If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.E.N' and aprobada = '1'").EOF Then
                                        'Rss.AddNew
                                        'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                        'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                        'Rss.Fields("cu_tipohora") = "H.E.N"
                                        TempMonto = Recalcular_FormatoHoras(HorasExtrasNocturnas)
                                        'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                        Tipo = "H.E.N"
                                        MontoInsert = FormatNumber(TempMonto, 2)
                                        Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                    End If
                                End If
                                                             
                            Else
                                'Si es menor quiere decir que hubo horas faltantes, para calcularlas solo se necesita restar las horas trabajadas a
                                'las horas que debio haber trabajado segun el horario.
                                
                                'HorasHorario = Round(DateDiff("n", Rs2!HoraEntrada _
                                ', Rs2!InicioDescanso) / 60, 2) + Round( _
                                'DateDiff("n", Rs2!FinDescanso, Rs2!horasalida) / 60, 2)
                                
                                HorasFaltantes = HorasHorario - HorasTrabajadas
                                
                                'If HorasFaltantes > 10 Then
                                'Debug.Print "para"
                                'End If
                                
                                If Rs2!HF = 0 And HorasFaltantes > (G_MinutosSignificativos_HorasFaltantes / 60) Then
                                    Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '0'")
                                    If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '1'").EOF Then
                                        'Rss.AddNew
                                        'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                        'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                        'Rss.Fields("cu_tipohora") = "H.F"
                                        TempMonto = Recalcular_FormatoHoras(HorasFaltantes)
                                        'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                        Tipo = "H.F"
                                        MontoInsert = FormatNumber(TempMonto, 2)
                                        Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")

                                        If HorasFaltantes > (G_MinutosSignificativos_Dia / 60) Then
                                            If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 1 & ")"
                                                Conexion.Execute sql
                                            End If
                                        ElseIf HorasFaltantes > (G_MinutosSignificativos_Medio_Dia / 60) Then
                                            If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 0.5 & ")"
                                                Conexion.Execute sql
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            
                        Else
                        
                                'HorasFaltantes = Abs(HorasHorario) 'no entiendo esta condicion siempre va a poner inasistente a la persona.
                                HorasFaltantes = HorasHorario - HorasTrabajadas
                                                                
                                Conexion.CommandTimeout = 0
                                'Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '0'")
                                
                                'If HorasFaltantes > (G_MinutosSignificativos_HorasFaltantes / 60) Then
                                    If Rs2!HF = 0 Then
                                       If Conexion.Execute("SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '1'").EOF Then
                                            'Rss.AddNew
                                            'Rss.Fields("cu_codpersonal") = Rs!CodigoEmpleado
                                            'Rss.Fields("du_fechacorte") = Mid(i, 1, 10)
                                            'Rss.Fields("cu_tipohora") = "H.F"
                                            TempMonto = Recalcular_FormatoHoras(HorasFaltantes)
                                            'Rss.Fields("nu_monto") = FormatNumber(TempMonto, 2)
                                            medioDiaEvaluado = True
                                            Tipo = "H.F"
                                            MontoInsert = FormatNumber(TempMonto, 2)
                                            Conexion.Execute ("DELETE FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechacorte = '" & Mid(i, 1, 10) & "' AND cu_tipohora = 'H.F' and aprobada = '0'")
                                            Conexion.Execute ("Insert into TR_NOMINA_HORASEXTRAS_POR_APROBAR (cu_codpersonal,du_fechacorte,cu_tipohora,nu_monto) Values ('" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','" & Tipo & "'," & MontoInsert & ")")
                                            
                                            If HorasFaltantes > (G_MinutosSignificativos_Dia / 60) Then
                                                If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                    sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                    "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 1 & ")"
                                                    Conexion.Execute sql
                                                End If
                                            ElseIf HorasFaltantes > (G_MinutosSignificativos_Medio_Dia / 60) Then
                                                If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "'  AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & 0.5 & ")"
                                                        Conexion.Execute sql
                                                End If
                                            End If
                                        End If
                                    End If
                                'End If
                        
                            Select Case G_Accion_LlegadaTardia
                            
                                Case 0
                                
                                    'No se toma ninguna acci�n. Se deja a criterio del cliente
                                    'evaluar las horas faltantes y colocar inasistencia
                                
                                Case 1, 2
                                
                                    If G_Accion_LlegadaTardia = 1 Then
                                        ValorTemp = 0.5
                                    Else
                                        ValorTemp = 1
                                    End If
                                
                                    '1 Es para medio d�a y 2 el d�a completo, pero como la validaci�n
                                    'se refiere a si el empleado fallo el tiempo de gracia, y
                                    'se sobreentiende que esta trabajando solo medio d�a, entonces
                                    'se le graba inasistencia completa.
                                                      
                                    If Conexion.Execute("SELECT * FROM TR_ASISTENCIA WHERE cu_codpersonal = '" & rs!CodigoEmpleado & "' AND du_fechaAsistencia = '" & Mid(i, 1, 10) & "' AND cu_codusuario = 'Auto' AND cu_codAsistenciatipo = '0000000002'").EOF Then
                                        sql = "INSERT INTO TR_ASISTENCIA (cu_codnomina, cu_codpersonal, du_fechaAsistencia, cu_codAsistenciatipo, cu_codusuario, ds_fecha_modificacion, nu_valor)" & _
                                        "values('" & Rs2!codnomina & "','" & rs!CodigoEmpleado & "','" & Mid(i, 1, 10) & "','0000000002','AUTO', '" & Mid(Now(), 1, 10) & "'," & ValorTemp & ")"
                                        Conexion.Execute sql
                                    End If
                                    
                            End Select
                        
                        End If
                    End If

            End If
            'Rss.UpdateBatch
            'Rss.Close
            Rs2.Close
        Next i
        'Rss.UpdateBatch
        'Rss.Close
        Rs3.Close
        rs.MoveNext
    Wend

    'Rss.UpdateBatch
    'Rss.Close
    rs.Close
    
End Sub

Public Sub RedondearHoras()

    If ExisteReglaNegocios("Rangos_Fraccion_hora_especial") Then
        
        mValorRegla = Obtener_Valor_Regla("Rangos_Fraccion_hora_especial")
        marr = Split(mValorRegla, ";")
        
        SQL1 = "update TR_NOMINA_HORASEXTRAS_POR_APROBAR " _
        & " Set nu_monto = Round(nu_monto, 0) " _
        & " where (nu_monto - round(nu_monto,0,1)) between " & marr(0) & " and " & marr(1) & ""
        
        SQL2 = "update TR_NOMINA_HORASEXTRAS_POR_APROBAR " _
        & " Set nu_monto = Round(nu_monto, 0) " _
        & " where (nu_monto - round(nu_monto,0,1)) between " & marr(1) & " and " & marr(2) & ""
        
        SQL3 = "update TR_NOMINA_HORASEXTRAS_POR_APROBAR " _
        & " Set nu_monto = Round(nu_monto, 0) " _
        & " where (nu_monto - round(nu_monto,0,1)) between " & marr(2) & " and " & marr(3) & ""
        
        Conexion.Execute SQL1
        Conexion.Execute SQL2
        Conexion.Execute SQL3
    
    End If

End Sub

Public Function Existe_Tabla(Tabla As String) As Boolean
    
    Dim rs As New ADODB.Recordset
    
    On Error GoTo Error
    
    sql = "select * from " & Tabla
    rs.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly
    rs.Close
    Existe_Tabla = True
    
    Exit Function
    
Error:
    
    Existe_Tabla = False
    
End Function

Public Function Obtener_Valor_Regla(Regla As String, Optional pDefault As String = "") As String

    Dim rs As New ADODB.Recordset
    
    sql = "SELECT valor as valor From MA_REGLASDENEGOCIOS where regla = '" & Regla & "'"
    rs.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rs.EOF Then
        Obtener_Valor_Regla = CStr(rs!Valor)
    Else
        Obtener_Valor_Regla = pDefault
    End If
    
    rs.Close
    
End Function

Public Function DevolverCortes(pCodNomina As String, pFechaIni As Date, pFechaFin As Date) As Variant

    On Error GoTo err

    Dim mValores As Variant

    Dim TempFechaI As Date
    Dim TempFechaF As Date

    Dim RsFrecuencia As New ADODB.Recordset
    Dim sql As String

    sql = "SELECT MA_NOMINA.cu_codigo AS CodNomina, MA_SALARIO_FRECUENCIA.* FROM MA_NOMINA" _
    & vbNewLine & "INNER JOIN MA_SALARIO_FRECUENCIA ON" _
    & vbNewLine & "MA_NOMINA.cu_codSalarioFecuencia = MA_SALARIO_FRECUENCIA.cu_codigo" _
    & vbNewLine & "WHERE MA_NOMINA.cu_codigo = '" & pCodNomina & "'"
    
    Debug.Print sql

    RsFrecuencia.Open sql, Conexion, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not RsFrecuencia.EOF Then
    
        Select Case RsFrecuencia!cu_codigo
        
            Case "0000000004"
            
                If ExisteReglaNegocios("Periodos_CortesNomina_QUINCENAL") Then
                
                    Dim mFechaActMenosUnMes As String
                    Dim mFechaActMasUnMes   As String
                    
                    mValorRegla = Obtener_Valor_Regla("Periodos_CortesNomina_QUINCENAL")
                    marr = Split(mValorRegla, ";")
                    
                    mFechaActMenosUnMes = DateAdd("m", -1, CDate(pFechaIni))
                    mFechaActMasUnMes = DateAdd("m", 1, CDate(pFechaFin))
                    
                    If Day(pFechaIni) = "1" Then
                        TempFechaI = marr(0) & "/" & Month(mFechaActMenosUnMes) & "/" & Year(mFechaActMenosUnMes)
                        TempFechaF = marr(1) & "/" & Month(pFechaFin) & "/" & Year(pFechaFin)
                    Else
                        TempFechaI = marr(2) & "/" & Month(pFechaIni) & "/" & Year(pFechaIni)
                        TempFechaF = marr(3) & "/" & Month(pFechaFin) & "/" & Year(pFechaFin)
                    End If
                    
                    mValores = Split(Format(TempFechaI, "DD/MM/YYYY") & "|" & Format(TempFechaF, "DD/MM/YYYY"), "|")
                
                Else
                
                    mValores = Split(Format(pFechaIni, "DD/MM/YYYY") & "|" & Format(pFechaFin, "DD/MM/YYYY"), "|")
                
                End If
                
'            Case "0000000002"
'
'                If ExisteReglaNegocios("Periodos_CortesNomina_MENSUAL") Then
'
'                    Dim mFechaActMenosUnMes1 As String
'                    Dim mFechaActMasUnMes1   As String
'
'                    mValorRegla = Obtener_Valor_Regla("Periodos_CortesNomina_MENSUAL")
'                    marr = Split(mValorRegla, ";")
'
'                    mFechaActMenosUnMes1 = DateAdd("m", -1, CDate(pFechaIni))
'                    mFechaActMasUnMes1 = DateAdd("m", 1, CDate(pFechaFin))
'
'                    TempFechaI = marr(0) & "/" & Month(mFechaActMenosUnMes1) & "/" & Year(mFechaActMenosUnMes1)
'                    TempFechaF = marr(1) & "/" & Month(pFechaFin) & "/" & Year(pFechaFin)
'
'                    mValores = Split(Format(TempFechaI, "DD/MM/YYYY") & "|" & Format(TempFechaF, "DD/MM/YYYY"), "|")

            
            Case Else
            
                mValores = Split(Format(pFechaIni, "DD/MM/YYYY") & "|" & Format(pFechaFin, "DD/MM/YYYY"), "|")
            
        End Select
        
    Else
    
        mValores = Split(Format(pFechaIni, "DD/MM/YYYY") & "|" & Format(pFechaFin, "DD/MM/YYYY"), "|")
    
    End If

    DevolverCortes = mValores

    Exit Function

err:

    DevolverCortes = Split(Format(pFechaIni, "DD/MM/YYYY") & "|" & Format(pFechaFin, "DD/MM/YYYY"), "|")

End Function

Public Function ObtenerUltimaEjecucion(ByVal pFechaIni As Date, ByVal codigoEmple As String) As Date

    Dim rs As New ADODB.Recordset
    Dim Rs2 As New ADODB.Recordset
    Dim sql As String
    
    On Error GoTo Error1
    
        sql = "SELECT max (du_fechaCorte) as fecha " _
        & "FROM tr_nomina_horasExtras where cu_codpersonal ='" & codigoEmple & "' "
        
        rs.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly
        
        ' Esto es una funcion de agregado si no hay nada trae null
        Debug.Print sql
        If IsNull(rs!fecha) Then
            ObtenerUltimaEjecucion = pFechaIni
        Else
            If CDate(rs!fecha) < CDate(pFechaIni) Then
                ObtenerUltimaEjecucion = pFechaIni
            Else
                ObtenerUltimaEjecucion = DateAdd("d", 1, CDate(rs!fecha))
            End If
        End If
        
        rs.Close
        
        sql = "SELECT max (du_fechaCorte) as fecha " _
        & "FROM tr_nomina_horasExtras_por_aprobar where cu_codpersonal ='" & codigoEmple & "'"
        
        rs.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly
        
        If IsNull(rs!fecha) Then
            ObtenerUltimaEjecucion = ObtenerUltimaEjecucion
        Else
            If CDate(rs!fecha) < CDate(ObtenerUltimaEjecucion) Then
                ObtenerUltimaEjecucion = ObtenerUltimaEjecucion
            Else
                ObtenerUltimaEjecucion = DateAdd("d", 1, CDate(rs!fecha))
            End If
        End If
        
        rs.Close
    
    Exit Function

Error1:

    MsgBox ObtenerUltimaEjecucion

End Function

Public Function ExisteReglaNegocios(pRegla As String) As Boolean
    
    Dim rs As New ADODB.Recordset
    Dim sql As String
    
    sql = "SELECT * " _
    & "FROM MA_REGLASDENEGOCIOS " _
    & "WHERE regla='" & pRegla & "' "
    
    rs.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly
    ExisteReglaNegocios = Not rs.EOF
    rs.Close

End Function

Public Function LimpiarBaseDeDatos()

    Dim SqlBaseproc As String
    Dim SqlHorasPorAprobar As String
    Dim SqlAsistencia As String
    Dim SqlTrImp As String
    Dim SqlHorasExtras As String
    
    On Error GoTo errbd
    
    Conexion.BeginTrans
       
    SqlTrImp = "SELECT * INTO tr_impotamarcajes2 FROM TR_IMPORTAMARCAJE group by codigoemple,diamarca,horamarca,biometrico,migrado,obviar"
       
    SqlBaseproc = "SELECT * INTO Baseproc2 " _
    & "FROM Baseproc group by bas_asistencia, bas_claves,bas_clasificacion,bas_claves,bas_fechas," _
    & "bas_horass,bas_Observacion,bas_tipo_registro,bas_biometrico"
    
    SqlHorasPorAprobar = "SELECT * INTO TR_NOMINA_HORASEXTRAS_POR_APROBAR2 " _
    & "FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR group by cu_codpersonal,cu_tipohora," _
    & "du_fechacorte,nu_monto,Aprobada"
    

    
    SqlAsistencia = "SELECT * INTO TR_ASISTENCIA2 " _
    & "FROM TR_ASISTENCIA group by cu_codAsistenciatipo,cu_codnomina,cu_codpersonal,cu_codusuario," _
    & "du_fechaAsistencia , ds_fecha_modificacion,nu_valor"
    
    Conexion.Execute SqlTrImp
    Conexion.Execute SqlBaseproc
    Conexion.Execute SqlHorasPorAprobar
    Conexion.Execute SqlAsistencia
    
    SqlTrImp = "TRUNCATE TABLE TR_IMPORTAMARCAJE"
    
    SqlBaseproc = "TRUNCATE TABLE Baseproc"
    
    SqlHorasPorAprobar = "TRUNCATE TABLE TR_NOMINA_HORASEXTRAS_POR_APROBAR"
        
    SqlAsistencia = "TRUNCATE TABLE TR_ASISTENCIA"
    

    
    Conexion.Execute SqlTrImp
    Conexion.Execute SqlBaseproc
    Conexion.Execute SqlHorasPorAprobar
    Conexion.Execute SqlAsistencia
 
    
    
    SqlTrImp = "INSERT INTO TR_IMPORTAMARCAJE SELECT * FROM tr_impotamarcajes2"
    
    SqlBaseproc = "INSERT INTO Baseproc " _
    & "SELECT * FROM Baseproc2"
    
    SqlHorasPorAprobar = "INSERT INTO TR_NOMINA_HORASEXTRAS_POR_APROBAR " _
    & "SELECT * FROM TR_NOMINA_HORASEXTRAS_POR_APROBAR2"
    
    SqlAsistencia = "INSERT INTO TR_ASISTENCIA " _
    & "SELECT * FROM TR_ASISTENCIA2"

    Conexion.Execute SqlTrImp
    Conexion.Execute SqlBaseproc
    Conexion.Execute SqlHorasPorAprobar
    Conexion.Execute SqlAsistencia

    
    SqlTrImp = "drop table tr_impotamarcajes2"
    SqlBaseproc = "drop table Baseproc2"
    SqlHorasPorAprobar = "drop table tr_nomina_horasextras_por_aprobar2"
    SqlAsistencia = "drop table tr_asistencia2"

    
    Conexion.Execute SqlTrImp
    Conexion.Execute SqlBaseproc
    Conexion.Execute SqlHorasPorAprobar
    Conexion.Execute SqlAsistencia

    
    Conexion.CommitTrans
    
    Exit Function
    
errbd:
       Debug.Print err.Description
       Conexion.RollbackTrans
       MsgBox "No se puede localizar el servidor,base de datos u Tabla", vbExclamation, "Informaci�n Stellar"
        End

End Function

Public Function Obtener_Rango_ConfiguracionBio(Optional pDefault As String = "") As Double

    Dim rs As New ADODB.Recordset
    
    
    sql = "SELECT Rango as Rango From MA_ConfiguradorMarcajes"
    rs.Open sql, Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not rs.EOF Then
        Obtener_Rango_ConfiguracionBio = CDbl(rs!Rango)
    Else
        Obtener_Rango_ConfiguracionBio = pDefault
    End If
    
    rs.Close
    
End Function

Public Function VerificaSiEjecutar()
    
    Dim RsHorasExtras           As New ADODB.Recordset
    Dim rs                      As New ADODB.Recordset
    Dim SqlCadena               As String
    Dim FechaHorasExtras        As Date
    Dim FechaDeCorte            As Date
    Dim mVar                    As Variant
    
    
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, True)
    
    SqlCadena = "select top(1) du_fechacorte from TR_NOMINA_HORASEXTRAS" _
        & " order by du_fechacorte desc"
        
        RsHorasExtras.Open SqlCadena, Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
        
    FechaHorasExtras = CDate(Format(RsHorasExtras!du_fechacorte, "dd/mm/yyyy"))
    Debug.Print FechaHorasExtras
    
    RsHorasExtras.Close
    
          SqlCadena = "SELECT top(1) CodigoEmpleado, Nomina, FechaDesde, CAST(floor ( CAST( FechaHasta AS FLOAT ) ) AS DATETIME) as FechaHasta, Rango, PrimerMarcaje1, UltimoMarcaje1, PrimerMarcaje2, UltimoMarcaje2, MarcajeAObviar " _
        & "FROM (SELECT CodigoEmpleado, Nomina1 AS Nomina, CASE WHEN Nomina2 IS NULL THEN FechaDesde ELSE CASE WHEN CAST(FechaDesde AS DATETIME)  " _
        & "> CAST(UltimaCorrida AS DATETIME) THEN CAST(FechaDesde AS DATETIME) ELSE CAST(UltimaCorrida AS DATETIME) END END AS FechaDesde,  " _
        & "CAST(ISNULL(FechaHasta, dateadd (d,-1,GETDATE())) AS DATETIME) AS FechaHasta, Rango, PrimerMarcaje1, UltimoMarcaje1, PrimerMarcaje2, UltimoMarcaje2,  " _
        & "MarcajeAObviar FROM (SELECT TB1.CodigoEmpleado, TB1.Nomina1, TB1.FechaDesde, TB1.FechaHasta, TB1.Rango, TB1.PrimerMarcaje1,  " _
        & "TB1.UltimoMarcaje1, TB1.PrimerMarcaje2, TB1.UltimoMarcaje2, TB1.MarcajeAObviar, TB2.Nomina2, TB2.UltimaCorrida " _
        & "FROM (SELECT  TR_IMPORTAMARCAJE.codigoemple AS CodigoEmpleado, MA_PERSONAL.cu_codnomina AS Nomina1, MIN(MA_CorrelativosMarcajes.fDesde) AS  " _
        & "FechaDesde, MA_CorrelativosMarcajes.fHasta AS FechaHasta, MA_ConfiguradorMarcajes.Rango, MA_ConfiguradorMarcajes.PrimerMarcaje1,  " _
        & "MA_ConfiguradorMarcajes.UltimoMarcaje1, MA_ConfiguradorMarcajes.PrimerMarcaje2, MA_ConfiguradorMarcajes.UltimoMarcaje2, MA_ConfiguradorMarcajes.MarcajeAObviar " _
        & "FROM TR_IMPORTAMARCAJE INNER JOIN MA_PERSONAL ON TR_IMPORTAMARCAJE.codigoemple = MA_PERSONAL.cu_codigo INNER JOIN MA_CorrelativosMarcajes ON  " _
        & "MA_PERSONAL.cu_codigo = MA_CorrelativosMarcajes.CodigoStellar INNER JOIN MA_ConfiguradorMarcajes ON MA_CorrelativosMarcajes.Biometrico = MA_ConfiguradorMarcajes.Identificador " _
        & "GROUP BY TR_IMPORTAMARCAJE.codigoemple, MA_PERSONAL.cu_codnomina, MA_CorrelativosMarcajes.fHasta, MA_ConfiguradorMarcajes.Rango,  " _
        & "MA_ConfiguradorMarcajes.PrimerMarcaje1, MA_ConfiguradorMarcajes.UltimoMarcaje1, MA_ConfiguradorMarcajes.PrimerMarcaje2, MA_ConfiguradorMarcajes.UltimoMarcaje2,  " _
        & "MA_ConfiguradorMarcajes.MarcajeAObviar) AS TB1 LEFT OUTER JOIN (SELECT DISTINCT cu_codnomina AS Nomina2, MAX(ds_fechaCorrida) AS UltimaCorrida FROM  MA_NOMINA_PROCESADAS " _
        & "WHERE (cu_estatus = 'NOMINA') GROUP BY cu_codnomina, cu_estatus) AS TB2 ON TB1.Nomina1 = TB2.Nomina2) AS TB) AS T"
        
        '& IIf(G_Prueba, "", " WHERE (FechaDesde < FechaHasta)") '_
        '& " ORDER BY CodigoEmpleado, FechaDesde"

    Conexion.CommandTimeout = 0
    
    Debug.Print SqlCadena
    
    Set rs = Conexion.Execute(SqlCadena)
 
    
    
    mVar = DevolverCortes(CStr(rs!Nomina), CDate(rs!fechadesde), CDate(rs!FechaHasta))
        
    '    fdesde = mVar(0)
        FechaDeCorte = mVar(1)

    rs.Close
    Debug.Print FechaDeCorte
    If FechaDeCorte = FechaHorasExtras Then
        Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, False)
        End
    Else
        Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword, False)
    End If
End Function

