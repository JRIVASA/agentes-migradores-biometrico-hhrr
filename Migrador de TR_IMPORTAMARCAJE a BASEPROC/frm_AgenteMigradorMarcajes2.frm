VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frm_AgenteMigradorMarcajes2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7590
   ControlBox      =   0   'False
   Icon            =   "frm_AgenteMigradorMarcajes2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   360
   ScaleWidth      =   7590
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7590
      _ExtentX        =   13388
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   1
   End
End
Attribute VB_Name = "frm_AgenteMigradorMarcajes2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

    Dim rs As New ADODB.Recordset
    Dim Funciones As New Funciones
    
    'Aqui se llama al metodo ConectarBDD que se encarga de establecer las conexiones.
    Call Funciones.ConectarBDD
        
    Call LimpiarBaseDeDatos
    'basicamente lo que hace esta funcion es ver hay un dato guardado en tr_nomina_horasextra
    'que concuerde con la fecha de corte, de ser asi de el agente no se ejecutara
    'Call VerificaSiEjecutar
    
    'Se migran los marcajes a baseproc seg�n las configuraciones especificadas en la ficha de configuraci�n.
    Call Importar_A_Baseproc
    
    'Se guardan las asistencias y inasistencias
    Call CargarInasistencias
    
    'Se calculan las horas extras y faltantes
    Call CargarHorasExtrasYFaltantes
     'MsgBox "termino"
     
     'Evita duplicacio en la base de datos, agregado 20/11/14
     Call LimpiarBaseDeDatos
     
    Call RedondearHoras
      
    Debug.Print Format(Now, "hh:mm:ss")
    
    End
End Sub

