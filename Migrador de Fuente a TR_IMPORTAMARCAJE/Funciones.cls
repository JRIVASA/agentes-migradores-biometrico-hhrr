VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Public Function sgetini(SIniFile As String, SSection As String, SKey _
    As String, SDefault As String) As String
    Dim STemp As String * 256
    Dim NLength As Integer
    
    STemp = Space$(256)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, STemp, 255, SIniFile)
    sgetini = Left$(STemp, NLength)
End Function

Public Sub ConectarBDD()
    On Error GoTo Errores
    Dim setup As String
    setup = App.Path & "\setup.ini"

    G_Server_Local = sgetini(setup, "Server Local", "Servidor Local", "?")
    If G_Server_Local = "?" Then
        Call MsgBox("El servidor no se encuentra definido.", vbCritical, "Error")
        End
    End If
    
    G_BDServer_Local = sgetini(setup, "Server Local", "Base de Datos Local", "?")
    If G_BDServer = "?" Then
        Call MsgBox("La Base de Datos no se encuentra definida.", vbCritical, "Error")
        End
    End If
    
    G_ServerLogin = sgetini(setup, "Server Local", "Servidor Login", "SA")
    G_ServerPassword = sgetini(setup, "Server Local", "Servidor Password", "")
            
    G_MostrarProgreso = sgetini(setup, "Configuracion", "Mostrar Ventana", "0")
    G_Panalazo = sgetini(setup, "Configuracion", "Regla Panalazo", "")
    
    Call Crear_Conexiones(CStr(G_BDServer_Local), G_ServerLogin, G_ServerPassword)
    
    Exit Sub
Errores:
    EscribirErrores ("Se ha producido un error al ejecutar la aplicación: " & vbNewLine & Err.Description)
    'Call MsgBox(Err.Description, vbCritical, "Error")
End Sub
