Attribute VB_Name = "Modulo_Inicio"
Global Conexion As New ADODB.Connection

Global G_Server_Local
Global G_BDServer_Local
Global G_ServerLogin        As String
Global G_ServerPassword     As String

Global G_MostrarProgreso
Global G_Panalazo

Public Sub Crear_Conexiones(BaseDatos1 As String, SrvLogin As String, SrvPassword As String)
On Error GoTo ErrorAronio
    Dim ConexString1

    ConexString1 = "Provider=MSDataShape.1;Persist Security Info=False;Data Source=" & G_Server_Local & ";User ID=" & SrvLogin & ";Password=" & SrvPassword & ";Initial Catalog=" & BaseDatos1 & ";Data Provider=SQLOLEDB.1"
    If Conexion.State = adStateOpen Then Conexion.Close
    Conexion.ConnectionString = ConexString1
    If SrvLogin <> "" And G_BDServer_Local <> "" And G_Server_Local <> "" Then
        Conexion.Open
    Else
        MsgBox "No se ha definido el Servidor o la Base de Datos.", vbExclamation, "Información Stellar"
        End
    End If
       
Exit Sub
ErrorAronio:
    Debug.Print Err.Description
    MsgBox "No se puede localizar el servidor o la base de datos.", vbExclamation, "Información Stellar"
    End
End Sub

Function MigrarDatos(GRID As MSHFlexGrid)

'La Variable Biometrico funciona para el migraje manual de datos
    Dim Rs                  As New ADODB.Recordset
    Dim Rs2                 As New ADODB.Recordset
    Dim Rs3                 As New ADODB.Recordset
    Dim RsConfiguracion     As New ADODB.Recordset
    Dim Rss                 As New ADODB.Recordset
    Dim RsValidar           As New ADODB.Recordset
    
    Dim Cnn As New ADODB.Connection
    Dim numLineas As Double
    
    Dim Errores As String
    
    Dim Migrado As Boolean
    
On Error GoTo Error:

    If Not G_Panalazo = "" Then
          Conexion.Execute ("update ma_correlativosmarcajes set fdesde = '" & G_Panalazo & "'")
    End If
    
    Set RsConfiguracion = Conexion.Execute("Select * from MA_ConfiguradorMarcajes")
    
    'Aqui se crea una tabla para transferir los marcajes desde la fuente a la base de datos, para luego poder verificar los que esten en
    'TR_IMPORTAMARCAJE y asi solo migrar los que no esten, evitando asi la duplicidad de datos.
    sql = "IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TR_IMPORTAMARCAJE_TEMPORAL') BEGIN " _
        & "CREATE TABLE [dbo].[TR_IMPORTAMARCAJE_TEMPORAL]( " _
        & "[codigoemple] [nvarchar](20) NOT NULL, " _
        & "[diamarca] [nvarchar](30) NOT NULL, " _
        & "[horamarca] [nvarchar](30) NOT NULL, " _
        & "[biometrico] [nvarchar](50) NOT NULL, " _
        & "[migrado] [bit] NOT NULL DEFAULT 0 " _
        & ") ON [PRIMARY] END"
    
    Conexion.Execute (sql)
    
    Rss.CursorLocation = adUseServer
    If Rss.State = 1 Then Rss.Close
    Rss.Open "TR_IMPORTAMARCAJE_TEMPORAL", Conexion, adOpenStatic, adLockBatchOptimistic, adCmdTable
    
    'Estandarizamos la tabla MA_CorrelativosMarcajes, para sustituir los campos vacios por null
    'y asi todos sean iguales y puedan ser tratados correctamente a continuacion
    sql = "UPDATE MA_CorrelativosMarcajes Set fHasta = Null WHERE (fHasta = '')"
    Conexion.Execute (sql)
        
    While Not RsConfiguracion.EOF
        Dim flag As Boolean
        numLineas = 0
        'Separar los procesos segun sea el caso entre SQL, Excel, Access o Archivos de Texto
        Select Case RsConfiguracion!TipoArchivo
            
            Case "SQL"
            
                Cnn.ConnectionString = "Driver={SQL Server};Server=" & RsConfiguracion!Servidor & ";Database=" & RsConfiguracion!BaseDeDatos & ";Uid=" & RsConfiguracion!Login & ";Pwd=" & RsConfiguracion!Password & ";"
                If RsConfiguracion!fechaHora Then
                    cadenarecord = "select " & RsConfiguracion!campoUsuario & ", " & RsConfiguracion!campoFecha & " from " & RsConfiguracion!Tabla & " ORDER BY " & RsConfiguracion!campoFecha
                Else
                    cadenarecord = "select " & RsConfiguracion!campoUsuario & ", " & RsConfiguracion!campoFecha & ", " & RsConfiguracion!campoHora & " from " & _
                    Rs!Tabla & " ORDER BY " & RsConfiguracion!campoFecha & ", " & RsConfiguracion!campoHora
                End If
                
            Case "Archivo de Texto"
            
                Dim Linea As String, total As String
                Linea = ""
                total = ""
                If RsConfiguracion!Cabecero = False Then
                    Open RsConfiguracion!direccionArchivo For Input As #1
                    Do Until EOF(1)
                    Line Input #1, Linea
                    If Linea = "" Then
                        Bandera = True
                    End If
                    total = total + Linea + vbCrLf
                    Loop
                    Close #1
                    
                    Set FS = CreateObject("Scripting.FileSystemObject")
                    Set archivo = FS.CreateTextFile(RsConfiguracion!direccionArchivo, True)
                    If Bandera Then
                        archivo.Write (total)
                    Else
                        archivo.Write (vbCrLf & total)
                    End If
                    archivo.Close
                    
                Else
                
                    Open RsConfiguracion!direccionArchivo For Input As #1
                    Do Until EOF(1)
                    Line Input #1, Linea
                    If Linea <> "" Then
                        total = total + Linea + vbCrLf
                    End If
                    Loop
                    Close #1
                    
                    Set FS = CreateObject("Scripting.FileSystemObject")
                    Set archivo = FS.CreateTextFile(RsConfiguracion!direccionArchivo, True)
                    archivo.Write (total)
                    archivo.Close
                    
                End If

                flag = True
                DireccionLongitud = Split(CStr(RsConfiguracion!direccionArchivo), "\")
                direccionArchivo = ""
                For i = 0 To UBound(DireccionLongitud) - 1
                    direccionArchivo = direccionArchivo & DireccionLongitud(i) & "\"
                Next i
                direccionArchivo = Mid(direccionArchivo, 1, Len(direccionArchivo) - 1)
                
                Cnn.ConnectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};Dbq=" & direccionArchivo & "\;DefaultDir=" & direccionArchivo & "\;Extensions=asc,csv,tab,txt;"
                cadenarecord = "select * from " & DireccionLongitud(i) & ""
                
            Case "Archivo de Access"
                Cnn.ConnectionString = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" & RsConfiguracion!direccionArchivo & ";Uid="";Pwd="";"
                If RsConfiguracion!fechaHora Then
                    cadenarecord = "select " & RsConfiguracion!campoUsuario & ", " & RsConfiguracion!campoFecha & " from " & RsConfiguracion!Tabla & " ORDER BY " & RsConfiguracion!campoFecha
                Else
                    cadenarecord = "select " & RsConfiguracion!campoUsuario & ", " & RsConfiguracion!campoFecha & ", " & RsConfiguracion!campoHora & " from " & _
                    RsConfiguracion!Tabla & " ORDER BY " & RsConfiguracion!campoFecha & ", " & RsConfiguracion!campoHora
                End If
                
            Case "Archivo de Excel"
                flag = False
                If RsConfiguracion!Cabecero = False Then
                    Cnn.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=" & RsConfiguracion!direccionArchivo & ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1'"
                Else
                    Cnn.ConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0;Data Source=" & RsConfiguracion!direccionArchivo & ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'"
                End If
                cadenarecord = "select * from [" & RsConfiguracion!Tabla & "]"
                
        End Select
        
        Cnn.Open
        Set Rs = Cnn.Execute(cadenarecord)
            
        If Not Rs.EOF Then
            Call LlenarGrid(GRID, Rs, flag, RsConfiguracion)
            Cnn.Close
            
            If GRID.Cols = 2 Then
                GRID.Cols = 3
                For i = 0 To GRID.Rows - 2
                    GRID.TextMatrix(i, 2) = Mid(GRID.TextMatrix(i, 1), InStr(1, GRID.TextMatrix(i, 1), " ") + 1)
                    a = InStr(1, GRID.TextMatrix(i, 1), " ") - 1
                    
                    '06/04/2011 se agrego una validacion para no dar un parametro invalido a la funcion que esta comentada
                    If a < 0 Then a = 0
                    
                    GRID.TextMatrix(i, 1) = Mid(GRID.TextMatrix(i, 1), 1, a)
'                    GRID.TextMatrix(i, 1) = Mid(GRID.TextMatrix(i, 1), 1, InStr(1, GRID.TextMatrix(i, 1), " ") - 1)
'
                    GRID.TextMatrix(i, 1) = Format(GRID.TextMatrix(i, 1), "dd/mm/yyyy")
                    GRID.TextMatrix(i, 2) = Format(GRID.TextMatrix(i, 2), "HH:mm:ss")
                Next i
            End If
            
            'Colocar filtros de fechas, fDesde mayor al inicio de nomina o FHasta en valor null o mayor al inicio de la nomina
            sql = "select CodigoStellar, fDesde, fHasta from MA_CorrelativosMarcajes Where Biometrico = '" & RsConfiguracion!Identificador & "'"
            Set Rs2 = Conexion.Execute(sql)
                      
            If Not Rs2.EOF Then
                existenCorrelativos = True
                numLineas = GRID.Rows
                frm_AgenteMigradorMarcajes.ProgressBar.Max = numLineas
                frm_AgenteMigradorMarcajes.ProgressBar.Value = 1

                If G_MostrarProgreso = "1" Then
                    frm_AgenteMigradorMarcajes.Show
                End If

                For i = 0 To GRID.Rows - 2
                    If frm_AgenteMigradorMarcajes.ProgressBar.Value = numLineas Then
                        frm_AgenteMigradorMarcajes.ProgressBar.Value = 1
                    Else
                        frm_AgenteMigradorMarcajes.ProgressBar.Value = frm_AgenteMigradorMarcajes.ProgressBar.Value + 1
                    End If
                    If Rs.State = 1 Then Rs.Close
                    sql = "select CodigoStellar, Biometrico, fDesde, fHasta from MA_CorrelativosMarcajes where CodigoExterno = '" & GRID.TextMatrix(i, 0) & "'" & _
                            " and ((Cast('" & GRID.TextMatrix(i, 1) & "' as datetime) between fDesde and fHasta) or (CAST('" & GRID.TextMatrix(i, 1) & "' AS DATETIME) >= fDesde and fHasta is null)) and Biometrico = '" & RsConfiguracion!Identificador & "'"

                    Set Rs = Conexion.Execute(sql)

                    If Not Rs.EOF Then
                        Rss.AddNew
                        Rss("codigoemple") = Rs!CodigoStellar
                        Rss("diamarca") = GRID.TextMatrix(i, 1)
                        Rss("horamarca") = Replace((GRID.TextMatrix(i, 1) & " " & Format(GRID.TextMatrix(i, 2), "HH:mm:ss")), ".", "")
                        Rss("biometrico") = RsConfiguracion!Identificador
                        Rss("migrado") = 0
                    End If
                Next i
                Rss.UpdateBatch
            Else
                existenCorrelativos = False
                'Variable Mensaje Error Ya Mostrado para evitar recibir el mismo error varias veces.
                MsgBox "Error, no se han establecido los correlativos.", vbExclamation, "Información Stellar"
                GoTo SiguienteBiometrico
            End If
                           
            'Actualizar la frecuencia sino es un Migraje Manual es decir Biometrico <> ""
                Select Case Trim(RsConfiguracion!Frecuencia)
                    Case "Diario"
                        sql = "update MA_ConfiguradorMarcajes set FechaInicial = '" & CDate(RsConfiguracion!FechaInicial) + CDate(RsConfiguracion!CantidadFrecuencia) & "' where id = " & RsConfiguracion!ID
                        Conexion.Execute sql
                    Case "Semanal"
                        sql = "update MA_ConfiguradorMarcajes set FechaInicial = '" & CDate(RsConfiguracion!FechaInicial) + CDate(RsConfiguracion!CantidadFrecuencia) * 7 & "' where id = " & RsConfiguracion!ID
                        Conexion.Execute sql
                    Case "Mensual"
                        If RsConfiguracion!DiaDelMes <> "" Then
                            fechaNueva = DateAdd("m", RsConfiguracion!CantidadFrecuencia, RsConfiguracion!FechaInicial)
                            sql = "update MA_ConfiguradorMarcajes set FechaInicial = '" & fechaNueva & "' where id = " & RsConfiguracion!ID
                        Else
                            fechaNueva = DateAdd("m", RsConfiguracion!CantidadFrecuencia, RsConfiguracion!FechaInicial)
                            
                            While Weekday(CDate(fechaNueva)) <> RsConfiguracion!DiaSemanaDelMes
                                fechaNueva = CDate(fechaNueva) + 1
                            Wend
                            
                            While CInt(Mid(fechaNueva, 1, 2)) < ((RsConfiguracion!SemanaDelMes - 1) * 7)
                                fechaNueva = CDate("01" & Mid(fechaNueva, 3))
                                fechaNueva = fechaNueva + ((RsConfiguracion!SemanaDelMes - 1) * 7)
                                While Weekday(fechaNueva) <> RsConfiguracion!DiaSemanaMensual
                                    fechaNueva = CDate(fechaNueva) + 1
                                Wend
                            Wend
                            sql = "update MA_ConfiguradorMarcajes set FechaInicial = '" & fechaNueva & "' where id = " & RsConfiguracion!ID
                        End If
                        Conexion.Execute sql
                End Select
        Else
            MsgBox "Error, el origen de los datos no posee marcajes.", vbExclamation, "Información Stellar"
        End If
SiguienteBiometrico:
        RsConfiguracion.MoveNext
    Wend
    
    'Aqui se realiza una verificacion de los marcajes que estan migrados ya para solo migrar los nuevos, es decir los que no se hayan migrado.
    '19/08/2014 se agrego una validacion(distinct) para filtrar registros que pudieran venir repetidos desde la fuente.
    
'    SQL = "SELECT Distinct TR_IMPORTAMARCAJE_TEMPORAL.codigoemple, TR_IMPORTAMARCAJE_TEMPORAL.diamarca, TR_IMPORTAMARCAJE_TEMPORAL.horamarca,  " _
'        & "TR_IMPORTAMARCAJE_TEMPORAL.biometrico, TR_IMPORTAMARCAJE_TEMPORAL.migrado " _
'        & "FROM TR_IMPORTAMARCAJE_TEMPORAL LEFT OUTER JOIN " _
'        & "TR_IMPORTAMARCAJE ON TR_IMPORTAMARCAJE_TEMPORAL.horamarca = TR_IMPORTAMARCAJE.horamarca AND  " _
'        & "TR_IMPORTAMARCAJE_TEMPORAL.codigoemple = TR_IMPORTAMARCAJE.codigoemple " _

    'Consulta reformulada  30/10/2015,  para manejar temas de duplicacion de datos.Jesus
    
    sql = "SELECT TR_IMPORTAMARCAJE_TEMPORAL.codigoemple, TR_IMPORTAMARCAJE_TEMPORAL.horamarca,TR_IMPORTAMARCAJE_TEMPORAL.diamarca,TR_IMPORTAMARCAJE_TEMPORAL.biometrico " _
        & "FROM TR_IMPORTAMARCAJE_TEMPORAL " _
        & "where not exists (select TR_IMPORTAMARCAJE.codigoemple,TR_IMPORTAMARCAJE.horamarca from TR_IMPORTAMARCAJE where TR_IMPORTAMARCAJE.codigoemple=TR_IMPORTAMARCAJE_TEMPORAL.codigoemple and TR_IMPORTAMARCAJE.horamarca=TR_IMPORTAMARCAJE_TEMPORAL.horamarca) " _
        '& "TR_IMPORTAMARCAJE_TEMPORAL.codigoemple = TR_IMPORTAMARCAJE.codigoemple " _

    Debug.Print sql

       

    If Rs.State = 1 Then Rs.Close
    Set Rs = Conexion.Execute(sql)
    
    If Rss.State = 1 Then Rss.Close
    Rss.CursorLocation = adUseServer
    Rss.Open "TR_IMPORTAMARCAJE", Conexion, adOpenStatic, adLockBatchOptimistic, adCmdTable
    
    'Luego de tener solo los migrajes nuevos se procede a migrarlo para TR_IMPORTAMARCAJE
    If Not Rs.EOF Then
    'Debug.Print Rs.RecordCount
        While Not Rs.EOF
            Rss.AddNew
            Rss("codigoemple") = Rs!codigoemple
            Rss("diamarca") = Rs!diamarca
            Rss("horamarca") = Rs!horamarca
            Rss("biometrico") = Rs!biometrico
            Rss("migrado") = 0
            Rs.MoveNext
        Wend
        Rss.UpdateBatch
    End If
    
    Conexion.Execute ("DROP TABLE TR_IMPORTAMARCAJE_TEMPORAL")
    
    If Len(Errores) > 0 Then EscribirErrores (Errores)
    
Exit Function

Error:

Debug.Print Err.Description
Debug.Print Err.Number
If Err.Number = 76 Or Err.Number = 53 Then
    Errores = Errores & vbNewLine & "Ha ocurrido un error que ha impedido migrar los marcajes, no se encuentra el archivo " & RsConfiguracion!direccionArchivo & ". Verifique que el archivo exista y que se encuentre en esta ubicación."
ElseIf Err.Number = 9 Then
    Errores = Errores & vbNewLine & "El archivo " & RsConfiguracion!direccionArchivo & " contiene líneas en blanco. Por favor, elimine las líneas en blanco."
Else
    Errores = Errores & vbNewLine & "Ha ocurrido un error que ha impedido migrar los marcajes: " & Err.Description
End If

If Not RsConfiguracion.EOF Then
    GoTo SiguienteBiometrico
End If

If Len(Errores) > 0 Then EscribirErrores (Errores)

End Function

Private Function LlenarGrid(GRID As MSHFlexGrid, record As ADODB.Recordset, Texto As Boolean, Rs As ADODB.Recordset)

    GRID.Rows = 2
    GRID.Cols = record.Fields.Count
    GRID.ColWidth(0) = 1300
    
    j = 0
    If Not (record.BOF And record.EOF) Then
        record.MoveFirst
        While record.EOF = False
            For i = 1 To record.Fields.Count
                If Texto = True Then '-- tenemos q separarlo por su delimitador
                'Se extrae el dato de forma diferene si es CSV, porque debe ser tratado como
                'archivo de texto pero todo lo que haya entre las comas es considerado un
                'record field diferente
                    If (Mid(Rs!direccionArchivo, Len(Rs!direccionArchivo) - 2, 3) = "csv") Or (Mid(Rs!Tabla, Len(Rs!direccionArchivo), 1) = "$") Then
                        Dato = ""
                        For a = 1 To record.Fields.Count
                        If a <> record.Fields.Count Then
                            Dato = Dato & record.Fields(a - 1) & ","
                        Else
                            Dato = Dato & record.Fields(a - 1)
                        End If
                        Next a
                    Else
                        Dato = IIf(IsNull(record.Fields(i - 1)), "0", record.Fields(i - 1))
                    End If
                    DatoSep = Split(Dato, CStr(Rs!delimitador))
                    If Rs!campoUsuario <> "" And Rs!campoFecha <> "" And (Rs!campoHora <> "" Or Rs!fechaHora = True) Then
                        If Rs!fechaHora = True Then
                            GRID.Cols = 2
                            GRID.ColWidth(1) = 1750

                            'If DatoSEP(CInt(Rs!campoUsuario)) = "10405217" Then 'Debugging...
                                'Debug.Print "revisar"
                            'End If

                            GRID.TextMatrix(j, 0) = DatoSep(CInt(Rs!campoUsuario))
                            GRID.TextMatrix(j, 1) = Format(DatoSep(CInt(Rs!campoFecha)), "DD/MM/YYYY HH:mm:ss")
                        Else
                            GRID.Cols = 3
                            GRID.ColWidth(1) = 1000
                            GRID.ColWidth(2) = 1000
                            GRID.TextMatrix(j, 0) = DatoSep(CInt(Rs!campoUsuario))
                            GRID.TextMatrix(j, 1) = Format(DatoSep(CInt(Rs!campoFecha)), "DD/MM/YYYY")
                            GRID.TextMatrix(j, 2) = Format(DatoSep(CInt(Rs!campoHora)), "HH:mm:ss")
                        End If
                    Else
                        GRID.Cols = CInt(UBound(DatoSep)) + 1
                        For z = 0 To UBound(DatoSep)
                            GRID.TextMatrix(j, z) = IIf(IsNull(DatoSep(z)), "0", DatoSep(z))
                        Next z
                    End If
                Else
                    GRID.TextMatrix(j, i - 1) = IIf(IsNull(record.Fields(i - 1)), "0", record.Fields(i - 1))
                End If
            Next i
            j = j + 1
            GRID.Rows = GRID.Rows + 1
            record.MoveNext
        Wend
    End If
    If GRID.Rows > 2 Then
        GRID.Rows = GRID.Rows - 1
    End If
            
End Function

Public Sub EscribirErrores(pError As String, Optional pMostrarFecha As Boolean = True)
    On Error GoTo Error
    Dim mCanal As Integer
    
    mCanal = FreeFile
        
    If pMostrarFecha Then pError = Format(Now, "DD/MM/YYYY hh:mm:ss AMPM") & pError
        
    Open App.Path & "\LogError.txt" For Output Access Write As #mCanal
    Print #mCanal, pError
    Close #mCanal
        
    Exit Sub
Error:
   Debug.Print Err.Description
        
End Sub
