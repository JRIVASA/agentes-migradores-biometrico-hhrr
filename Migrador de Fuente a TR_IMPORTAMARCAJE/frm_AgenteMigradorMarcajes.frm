VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_AgenteMigradorMarcajes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Migrando marcajes de la fuente a la Base de Datos..."
   ClientHeight    =   360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7590
   ControlBox      =   0   'False
   Icon            =   "frm_AgenteMigradorMarcajes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_AgenteMigradorMarcajes.frx":628A
   ScaleHeight     =   360
   ScaleWidth      =   7590
   StartUpPosition =   2  'CenterScreen
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid GRID 
      Height          =   30
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   30
      _ExtentX        =   53
      _ExtentY        =   53
      _Version        =   393216
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7590
      _ExtentX        =   13388
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   1
   End
End
Attribute VB_Name = "frm_AgenteMigradorMarcajes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

    Dim Rs As New ADODB.Recordset
    Dim Funciones As New Funciones
    
    'Aqui se llama al metodo ConectarBDD que se encarga de establecer las conexiones.
    Call Funciones.ConectarBDD
    
    'Luego se llama al metodo que se encargará de migrar los datos desde la fuente hasta la tabla TR_IMPORTAMARCAJE
    Call MigrarDatos(GRID)
    
    Conexion.Execute ("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'TR_IMPORTAMARCAJE_TEMPORAL') BEGIN DROP TABLE TR_IMPORTAMARCAJE_TEMPORAL END")
    
    End
End Sub

Private Sub ProgressBar_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

End Sub
